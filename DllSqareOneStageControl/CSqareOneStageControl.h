/**
 * Sqare One Stage Control Class
 *
 * @Class  CSqareOneStageControl
 * @Date   2021/06/09
 * @Author jsjeong // jsjeong@euvsol.com
 * @Brief  Sqare One Stage Control
 *
 **/

#include <list>
#pragma once

#define IP_ADDRESS_DEFINE _T("127.0.0.1")
#define IP_LVPORT_DEFINE    50122
#define Xleftlimit -15.0
#define Xrightlimit 15.0	
#define Yleftlimit -10.0
#define Yrightlimit 10.0
#define Zleftlimit -15.0
#define Zrightlimit 15.0
#define Rxleftlimit -10.0
#define Rxrightlimit 10.0
#define Ryleftlimit -17.0
#define Ryrightlimit 17.0
#define Rzleftlimit -15.0
#define Rzrightlimit 15.0

class AFX_EXT_CLASS CSqareOneStageControl : public CEthernetCom
{
public:
	CSqareOneStageControl(void);
	~CSqareOneStageControl(void);

	HANDLE m_hGetReturnEvent;
	HANDLE m_hSetReturnEvent;
	HANDLE m_hStopReturnEvent;
	HANDLE m_hMoveCompleteEvent;

	struct IpConfig 
	{
		char* ip;
		int port;
	};
	IpConfig ipconfig;

	char m_storeBuff[540]; //< 통신 끊김용 버퍼
	int m_nRemianBuffIndex; //< 통신 끊긴 버퍼 위치 인덱스
	bool m_bStopFlag;
	bool m_isMoving;
	typedef struct _SQ_DATA
	{ //< SQ-1 스테이지 데이터 저장 구조체
		double X = 0;
		double Y = 0;
		double Z = 0;
		double Rx = 0;
		double Ry = 0;
		double Rz = 0;
		double Cx = 0;
		double Cy = 0;
		double Cz = 0;
		bool TrisphereStatus = 0;
		bool TargetExceedRanged = 0;
		bool MoveComplete = 0;
		bool VaildControlPoint = 0;
	} SQ_DATA;
	SQ_DATA	m_SQOneParseData; //< 파싱 데이터 구조체 변수
	SOCKET TmpSocket = INVALID_SOCKET; //< 소켓 설정 변수

	virtual	int	ReceiveData(char *lParam); //<데이터 수신 함수

	//basic function
	int sendGetCommand(); //< Get 명령 전송 함수
	int sendSetCommand(double dCommandX, double dCommandY, double dCommandZ, double dCommandRx, double dCommandRy, double dCommandRz, double dCommandCx, double dCommandCy, double dCommandCz); //< Set 명령 전송 함수
	int sendStopCommand(); //< Stop 명령 전송 함수

	//Data handling
	void makeDataSet(char *lParam); //< 파싱 데이터 세트 설정 함수
	int parseData(char * bufrecv); //< 데이터 파싱 함수

	//Motion Handling
	int waitUntilMoveComplete(double dCommandX, double dCommandY, double dCommandZ, double dCommandRx, double dCommandRy, double dCommandRz, double dCommandCx, double dCommandCy, double dCommandCz); //< Stage 구동 완료 대기 함수
	
	bool m_bOldMoveCompleteFlag;

};

