#ifndef _MEMDC_H_
#define _MEMDC_H_
//////////////////////////////////////////////////
// 이 클래스는 메모리 Device Context
class CMemmDC : public CDC 
{
private:
        CBitmap m_bitmap; // 화면 바깥 비트맵
        CBitmap* m_oldBitmap; // 원래 CMemDC에서 찾은 비트맵
        CDC* m_pDC; // 생성자로 넘어온 CDC를 저장한다
        CRect m_rect; // 그릴 부분의 사각형
        BOOL m_bMemDC; // CDC가 정말로 메모리 DC면 TRUE
public:
        CMemmDC(CDC* pDC) : CDC(), m_oldBitmap(NULL), m_pDC(pDC)
        {
            ASSERT(m_pDC != NULL); // 여기서 assert된다면 인자로 NULL CDC를 준 것이다
            
            m_bMemDC = !pDC->IsPrinting();
                
            if (m_bMemDC)
			{
                // 메모리 DC를 만든다
                CreateCompatibleDC(pDC);
                pDC->GetClipBox(&m_rect);
                m_bitmap.CreateCompatibleBitmap(pDC, m_rect.Width(), m_rect.Height());
                m_oldBitmap = SelectObject(&m_bitmap);
                SetWindowOrg(m_rect.left, m_rect.top);
       		} 
			else
			{
                // 인쇄를 위해 현재 DC의 관련된 부분을 복사한다
                m_bPrinting = pDC->m_bPrinting;
                m_hDC = pDC->m_hDC;
                m_hAttribDC = pDC->m_hAttribDC;
			}
        }
        
        ~CMemmDC()
        {
            if (m_bMemDC) 
			{
                // 화면 바깥 비트맵을 화면 위에 복사한다
                m_pDC->BitBlt(m_rect.left, m_rect.top, m_rect.Width(), m_rect.Height(),
                        this, m_rect.left, m_rect.top, SRCCOPY);
                // 원래의 비트맵으로 다시 바꾼다
                SelectObject(m_oldBitmap);
            }
			else 
			{
                // 우리가 해야하는 일은 DC를 잘못된 값으로 바꾸는 것이 전부이다.
                // 이것은 우리가 생성자에서 넘어온 CDC에 관련된 핸들을
                // 우연히 지우는 것을 방지해준다.
                m_hDC = m_hAttribDC = NULL;
            }
        }
        
        // 포인터로 사용하는 것을 허용한다
        CMemmDC* operator->() {return this;}
        
        // 포인터로 사용하는 것을 허용한다
        operator CMemmDC*() {return this;}
};
#endif
