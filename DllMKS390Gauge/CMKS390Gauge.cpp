#include "stdafx.h"
#include "CMKS390Gauge.h"


CMKS390Gauge::CMKS390Gauge()
{
	cnt = 0;
	m_dPressureLLC = 0.0;
	m_dPressureMC = 0.0;
	m_bLlcErrorState = FALSE;
	m_bMcErrorState = FALSE;
}


CMKS390Gauge::~CMKS390Gauge()
{
}

int CMKS390Gauge::ReceiveData(char *lParam, DWORD dwRead)
{
	CString strReceiveData;
	CString strChannel;
	CString strData;

	strReceiveData = (LPSTR)lParam;
	strChannel = strReceiveData.Mid(1, 2);
	
	// 2020.09.28
	// 센서 교체 (LLC <-> MC)
	//if (ch_value == "02") ch_2 = ReData.Mid(3, 10); //LLC
	//if (ch_value == "03") ch_3 = ReData.Mid(3, 10); //MC

	if (strChannel == _T("02"))
	{
		strData = strReceiveData.Mid(3, 10); //MC
		strData.TrimLeft();
		strData.TrimRight();

		int nCount = 0;
		int nPoint = 0;
		nPoint = strData.Find(' ');
		for (nCount = 0; nPoint != -1; nCount++)
			nPoint = strData.Find(' ', nPoint + 1);

		if (nCount > 0) //WHITE SPACE COUNT
		{
			if (strData != _T("00 ST OK"))
				m_bMcErrorState = TRUE;
			else
				m_bMcErrorState = FALSE;
		}
		else
		{
			m_dPressureMC = atof(strData);
		}
	}
	if (strChannel == _T("03"))
	{
		strData = strReceiveData.Mid(3, 10); //LLC
		strData.TrimLeft();
		strData.TrimRight();

		int nCount = 0;
		int nPoint = 0;
		nPoint = strData.Find(' ');
		for (nCount = 0; nPoint != -1; nCount++)
			nPoint = strData.Find(' ', nPoint + 1);

		if (nCount > 0)	//WHITE SPACE COUNT
		{
			if (strData != _T("00 ST OK"))
				m_bLlcErrorState = TRUE;
			else
				m_bLlcErrorState = FALSE;
		}
		else
		{
			m_dPressureLLC = atof(strData);
		}
	}

	if (cnt == 50)
	{
		CString sTemp;
		sTemp.Format(_T("PC -> MC_VacuumGauge : %.2e"), m_dPressureMC);
		SaveLogFile("MC_VacuumGauge_ReadLog", sTemp); //통신 상태 기록.
		sTemp.Format(_T("PC -> LLC_VaccumGauge : %.2e"), m_dPressureLLC);
		SaveLogFile("LLC_VacuumGauge_ReadLog", sTemp);	//통신 상태 기록.
		cnt = 0;
	}
	cnt++;

	return 0;
}

int CMKS390Gauge::SendData(char *lParam, int nTimeOut, int nRetryCnt)
{
	int nRet = 0;
	
	nRet = Send(lParam, nTimeOut, nRetryCnt);
	if (nRet != 0)
	{
		SaveLogFile("MC_VacuumGauge_ReadLog", _T((LPSTR)(LPCTSTR)("PC -> MC_VacuumGauge : Send Error ")));		//통신 상태 기록.
		SaveLogFile("LLC_VacuumGauge_ReadLog", _T((LPSTR)(LPCTSTR)("PC -> LLC_VaccumGauge : Send Error ")));	//통신 상태 기록.
	}

	return nRet;
}

int CMKS390Gauge::ReadMcVacuumRate()
{
	int nRet = Send("#02RD\r", 500);
	return nRet;
}

int CMKS390Gauge::ReadLlcVacuumRate()
{
	int nRet = Send("#03RD\r", 500);
	return nRet;
}

int CMKS390Gauge::ReadMcGaugeState()
{
	int nRet = Send("#02RS\r", 500);
	return nRet;
}

int CMKS390Gauge::ReadLlcGaugeState()
{
	int nRet = Send("#03RS\r", 500);
	return nRet;
}
