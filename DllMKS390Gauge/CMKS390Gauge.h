/**
 * MKS 390 Vaccum Gauge Control Class
 *
 * Copyright 2019 by E-SOL, Inc.,
 *
 **/

#pragma once
class AFX_EXT_CLASS CMKS390Gauge : public CSerialCom
{
public:
	CMKS390Gauge();
	~CMKS390Gauge();

	virtual int			ReceiveData(char *lParam, DWORD dwRead);
	virtual int			SendData(char *lParam, int nTimeOut = 0, int nRetryCnt = 3);

protected:

	BOOL m_bLlcErrorState;
	BOOL m_bMcErrorState;

	double m_dPressureLLC;
	double m_dPressureMC;

	int cnt;

	int ReadMcVacuumRate();
	int ReadLlcVacuumRate();
	int ReadMcGaugeState();
	int ReadLlcGaugeState();
};

