/**
 * Linear Revolver Control Class
 *
 * Copyright 2019 by E-SOL, Inc.,
 *
 **/
#pragma once

#include "./Include/LLC2_Changer.h"
#pragma comment(lib, "LLC2_Library.lib")

class AFX_EXT_CLASS CLinearRevolverCtrl : public CECommon
{
public:
	CLinearRevolverCtrl();
	~CLinearRevolverCtrl();

private:
	CLLC2_Changer* Dev;

public:
	/**
	*@fn int OpenSerialPort(CString sPortName)
	*@brief Device를 연다
	*@date 2019/08/09
	*@param sPortName 포트번호
	*@return 디바이스 연결 결과 값
	*/
	int OpenSerialPort(CString sPortName);

	/**
	*@fn int CloseDevice()
	*@brief Device를 닫는다
	*@date 2019/08/09
	*@return 디바이스 종료 결과 값
	*/
	int CloseDevice();

	/**
	*@fn void MoveHomeRevolver()
	*@brief 홈위치로 이동한다
	*@date 2019/08/09
	*/
	void MoveHomeRevolver();

	/**
	*@fn void MoveRight()
	*@brief 오른쪽으로 이동한다
	*@date 2019/08/09
	*/
	void MoveRight();

	/**
	*@fn void MoveLeft()
	*@brief 왼쪽으로 이동한다
	*@date 2019/08/09
	*/
	void MoveLeft();

	/**
	*@fn long CheckDirection()
	*@brief 현재방향을 확인한다
	*@date 2019/08/09
	*/
	long CheckDirection();

	/**
	*@fn long CheckStatus()
	*@brief 상태를 확인한다
	*@date 2019/08/09
	*/
	long CheckStatus();
};

