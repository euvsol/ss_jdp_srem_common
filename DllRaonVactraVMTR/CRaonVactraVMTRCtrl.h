/**
 * Raon Vactra-S2.0 VMTR(Vacuum Mask Transfer Robot) Control Class
 *
 * Copyright 2019 by E-SOL, Inc.,
 *
 **/
#pragma once

class AFX_EXT_CLASS CRaonVactraVMTRCtrl : public CEthernetCom
{
public:
	CRaonVactraVMTRCtrl();
	~CRaonVactraVMTRCtrl();

	virtual	int				SendData(char *lParam, int nTimeOut = 0, int nRetryCnt = 3);
	virtual	int				ReceiveData(char *lParam); ///< Recive

private:
	/**
	*@var HANDLE m_hAckEvent
	ACK 메시지 수신 이벤트
	*/
	HANDLE m_hAckEvent;

	/**
	*@var HANDLE m_hDoneEvent
	DONE 메시지 수신 이벤트
	*/
	HANDLE m_hDoneEvent;

	/**
	*@var HANDLE m_hReqEvent
	Request 메시지 수신 이벤트
	*/
	HANDLE m_hReqEvent;

	/**
	*@fn void ParsingReceivedMsg(CString strRecvMsg)
	*@brief 수신 받은 데이터 파싱
	*@date 2019/07/31
	*@param strRecvMsg 수신받은 메시지 내용
	*/
	void ParsingData(CString strRecvMsg);

	/**
	*@fn int WaitExecAckEvnet(int nTimeout)
	*@brief Ack 메시지 수신 타임아웃 체크
	*@date 2019/07/04
	*@param 타임아웃 시간
	*@return 0:시간내 응답, -1:타임아웃
	*/
	int WaitExecAckEvent(int nTimeout);

	/**
	*@fn int WaitExecDoneEvent(int nTimeout)
	*@brief Done 메시지 수신 타임아웃 체크
	*@date 2019/07/04
	*@param 타임아웃 시간
	*@return 0:시간내 응답, -1:타임아웃
	*/
	int WaitExecDoneEvent(int nTimeout);

	/**
	*@fn int WaitExecReqEvent(int nTimeout)
	*@brief Request 메시지 수신 타임아웃 체크
	*@date 2019/12/16
	*@param 타임아웃 시간
	*@return 0:시간내 응답, -1:타임아웃
	*/
	int WaitExecReqEvent(int nTimeout);

protected:
	/**
	*@var BOOL m_bConnect
	연결 확인 변수
	*/
	//BOOL m_bConnect;

	/**
	*@var BOOL m_bHomeComplete
	홈 완료 확인 변수
	*/
	BOOL m_bHomeComplete;

	/**
	*@var BOOL m_bReadyPosition
	준비 위치 확인 변수
	*/
	BOOL m_bReadyPosition;

	/**
	*@var BOOL m_bServoMotorOn
	서버모터 On/Off 확인 변수
	*/
	BOOL m_bServoMotorOn;

	BOOL m_bTempServoOn;
	
	/**
	*@var BOOL m_bRobotWorking
	로봇 동작 확인 변수
	*/
	BOOL m_bRobotWorking;
	
	/**
	*@var BOOL m_bChangeBattery
	배터리 교체 확인 변수
	*/
	BOOL m_bChangeBattery;
	
	/**
	*@var BOOL m_bRobotAutoMode
	로봇 모드(Auto/Teaching) 확인 변수
	*/
	BOOL m_bRobotAutoMode;
	
	/**
	*@var BOOL m_bEMOStatus
	EMO 상태 확인 변수
	*/
	BOOL m_bEMOStatus;
	
	/**
	*@var BOOL m_bOccurredErr
	에러발생 확인 변수
	*/
	BOOL m_bOccurredErr;

	BOOL m_bSendAvailable;

	/**
	*@var int m_nRobotSpeed
	로봇 속도 변수
	*/
	int	m_nRobotSpeed;

	/**
	*@var double m_dPositionZ
	현재 Z축 위치 변수
	*/
	double m_dPositionZ;
	
	/**
	*@var double m_dPositionT
	현재 T축 위치 변수
	*/
	double m_dPositionT;
	
	/**
	*@var double m_dPositionL
	현재 L축 위치 변수
	*/
	double m_dPositionL;

	/**
	*@var CString m_sErrorCode
	에러 코드 변수
	*/
	//CString m_sErrorCode;

	char m_sErrorCode[128];
	

	// 모니터 명령

	/**
	*@fn int ServoOn()
	*@brief 로봇의 서보모터 전원을 On
	*@date 2019/07/11
	*/
	int ServoOn();

	/**
	*@fn int ServoOff()
	*@brief 로봇의 서보모터 전원을 Off
	*@date 2019/07/11
	*/
	int ServoOff();
	
	/**
	*@fn void MoveRobotHome()
	*@brief 로봇을 원점 복귀 위치로 이동
	*@date 2019/07/11
	*@remark \n
			 최초 제어기 전원이 공급되고 Servo On이 되면 반드시 로봇은 Home 명령으로 원점 복귀를 해야한다
	*/
	int MoveRobotHome();
	
	/**
	*@fn int MoveRobotReady()
	*@brief 로봇암을 레뒤위치로 이동
	*@date 2019/07/11
	*@remark \n
			 로봇의 암축만 레뒤위치로 구동한다
	*/
	int MoveRobotReady();
	
	/**
	*@fn void RobotMovePause()
	*@brief 로봇의 동작을 일시정지
	*@date 2019/07/11
	*/
	void RobotMovePause();
	
	/**
	*@fn void RobotMoveResume()
	*@brief 일시정지된 로봇을 재구동
	*@date 2019/07/11
	*/
	void RobotMoveResume();
	
	/**
	*@fn void RobotMoveStop()
	*@brief 로봇의 동작을 정지
	*@date 2019/07/11
	*/
	void RobotMoveStop();
	
	/**
	*@fn void ReqRobotPosition()
	*@brief 로봇의 현재 위치를 요청
	*@date 2019/07/11
	*@remark \n
			 로봇의 각 축에 대한 현재 좌표 값은 직선 구동일 경우 0.001mm 단위이며 \n
			 회전 구동일 경우 0.001degree 단위가 된다 \n
			 * 응답된 값의 1000을 나누어 주면 mm 및 deg 단위가 된다 \n
			 [응답형식] POS Z100000 T90000 L150000<CR>
	*/
	void ReqRobotPosition();
	
	/**
	*@fn void ReqRobotStatus()
	*@brief 로봇의 현재 상태를 요청
	*@date 2019/07/11
	*@remark \n
			 [응답형식] STATUS 0000000000000000<CR> \n
			 * 데이터 형식 : 1234/5678/9/10/11/12/13/14/15/16 \n
			 1~4 : 에러코드 4자리 \n
			 5~8 : 마스크 유무, 마스크가 있으면 1 없으면 0을 반환 (5:Lower Arm, 6:Upper Arm, 7:3_Arm, 8:4_Arm) \n
			   9 : 홈 완료 상태를 반환 (1:완료, 0:미완료) \n
			  10 : 레뒤 위치 상태 (1:레뒤 위치, 0:레뒤 위치 아님) \n
			  11 : 서보 모터 온/오프 상태 (1:서보온, 0:서보오프) \n
			  12 : 로봇 구동 상태 (1:구동중, 0:정지중) \n
			  13 : 배터리 교체 시기 (1:배터리 교체, 0:정상) \n
			  14 : 자동 응답 사용 상태를 반환 (1:사용, 0:미사용) \n
			  15 : 로봇의 모드상태를 반환 (1:자동모드, 0:티칭모드) \n
			  16 : 비상정지 상태를 반환 (1:비상정지, 0:정상)
	*/
	void ReqRobotStatus();
	
	/**
	*@fn void ReqRobotPosStatus(CString strArm)
	*@brief 로봇의 현재 위치 상태를 요청
	*@date 2019/07/11
	*@param cArm 상태를 확인하고자 하는 Arm을 설정 (싱글/로워듀얼암 : L , 업퍼듀얼암 : U)
	*@remark \n
			 [응답 형식] PRESENT STATION [Stn] ARM [Arm] R [EX/RE] Z [Up/Down]<CR> \n
			 [Stn]: 현재 구동한 Station 번호를 반환 (0:알수없음) \n
			 [Arm]: 지정한 암을 반환 \n
			 (LL:Lower Left Arm, LR:Lower Right Arm, UL:Upper Left Arm, UR:Upper Right Arm) \n
			 [EX/RE]: 로봇의 해당 암상태를 반환 (EX:Extend, RE:Retract, 0:알수없음) \n
			 [Up/Down]: 로봇의 현재 Z축 상태를 반환 (UP:Up위치, DOWN:Down위치, 0:알수없음)
	*/
	void ReqRobotPosStatus(CString strArm);
	
	/**
	*@fn int ReqRobotSpeed()
	*@brief 로봇의 현재 동작 속도를 요청
	*@date 2019/07/11
	*@remark \n
			 로봇의 현재 속도를 확인 (단위 %) \n
			 [응답 형식] SPEED [Speed]<CR>
	*/
	int ReqRobotSpeed();
	
	/**
	*@fn void ReqStationInfo()
	*@brief 지정한 해당 Station의 정보를 요구한다
	*@date 2019/07/11
	*@param nStation 해당 Station 번호, strArg \n
			Arg : POS 는 설정한 Arm의 각 축 티칭 위치를 반환 \n
				  UP 은 지정한 Station의 Up Stroke에 대한 정보를 반환 \n
				  DOWN 은 지정한 Station의 Down Stroke에 대한 정보를 반환 \n
				  SLOT 은 지정한 Station의 최대 Slot의 정보를 반환 \n
				  PITCH 는 지정한 Station의 Pitch 정보를 반환 \n
				  SPEED 는 지정한 Station의 로봇 속도에 대한 정보를 반환 \n
				  MASK 는 지정한 Station의 마스크 두께에 대한 정보를 반환 \n
				  EDGE 는 지정한 Station의 Edge Offset에 대한 정보를 반환
	*@remark \n
			 로봇의 해당 Station의 대한 정보를 확인 할 수 있다 \n
			 (단위가 있는 파라메터는 0.001mm로 나타내며, 1000은 1mm, 23은 0.023mm을 의미)
	*/
	void ReqStationInfo(int nStation, CString strArg);

	/**
	*@fn int PickMask(int nStation)
	*@brief 해당 Station에서 마스크를 집는다
	*@date 2019/07/11
	*@param nStation Station번호 (1~50번)
	*@remark \n
			 [명령 형식] PICK 1 SLOT 1 ARM L<CR>
	*/
	int PickMask(int nStation);
	
	/**
	*@fn int PlaceMask(int nStation)
	*@brief 해당 Station에서 마스크를 놓는다
	*@date 2019/07/11
	*@param nStation Station번호 (1~50번), nSlot Slot번호(가장 아래쪽이 1번), cArm 구동시킬 로봇 암을 지정 (싱글암/로워듀얼암 : L, 업퍼암 : U)
	*@remark \n
			 [명령 형식] PLACE 2 SLOT 1 ARM L<CR>
	*/
	int PlaceMask(int nStation);
	
	/**
	*@fn int ExtendToPickMask(int nStation)
	*@brief 해당 Station에서 마스크를 집기위해 로봇암을 전진한다
	*@date 2019/07/11
	*@param nStation Station번호 (1~50번), nSlot Slot번호(가장 아래쪽이 1번), cArm 구동시킬 로봇 암을 지정 (싱글암/로워듀얼암 : L, 업퍼암 : U)
	*/
	int ExtendToPickMask(int nStation);
	
	/**
	*@fn int ExtendToPlaceMask(int nStation)
	*@brief 해당 Station에서 마스크를 투입하기 위해 로봇암을 전진한다
	*@date 2019/07/11
	*@param nStation Station번호 (1~50번), nSlot Slot번호(가장 아래쪽이 1번)
	*/
	int ExtendToPlaceMask(int nStation);
	
	/**
	*@fn int RetractToPickMask(int nStation)
	*@brief 해당 Station에 전진된 로봇암을 마스크 집을 위치에서 후퇴한다
	*@date 2019/07/11
	*@param nStation Station번호 (1~50번), nSlot Slot번호(가장 아래쪽이 1번), cArm 구동시킬 로봇 암을 지정 (싱글암/로워듀얼암 : L, 업퍼암 : U)
	*/
	int RetractToPickMask(int nStation);
	
	/**
	*@fn int RetractToPlaceMask(int nStation)
	*@brief 해당 Station에 전진된 로봇암을 마스크 투입되어야 할 위치에서 후퇴한다
	*@date 2019/07/11
	*@param nStation Station번호 (1~50번), nSlot Slot번호(가장 아래쪽이 1번), cArm 구동시킬 로봇 암을 지정 (싱글암/로워듀얼암 : L, 업퍼암 : U)
	*/
	int RetractToPlaceMask(int nStation);
	
	/**
	*@fn int MoveReservedPosition()
	*@brief 로봇의 동작을 단계적으로 구동 시킬 수 있는 명령이다
	*@date 2019/07/11
	*@param nStation Station번호 (1~50번), nSlot Slot번호(가장 아래쪽이 1번), cArm 구동시킬 로봇 암을 지정 (싱글암/로워듀얼암 : L, 업퍼암 : U) \n
			, strPosition 이동하려 하는 목표점을 의미 GET(G1~G5) PUT(P1~P5)
	*@remark \n
			 [명령 형식] GOTO [station] SLOT [slot] ARM [arm] R [G1/G2/G3/G4/G5/P1/P2/P3/P4/P5]<CR>
	*/
	int MoveReservedPosition(int nStation, CString strPosition);


	// 설정 명령

	/**
	*@fn void CheckCommStatus()
	*@brief 로봇이 정상적으로 통신되고 있는지 확인하는 목적으로 사용
	*@date 2019/07/11
	*/
	void CheckCommStatus();

	/**
	*@fn void SetStationTeachingData()
	*@brief 지정한 해당 Station의 정보를 변경한다
	*@date 2019/07/11
	*@param nStation 해당 Station 번호 , strArg \n
			Arg : POS는 설정한 Arm의 각 축 티칭 위치를 변경 \n
				  UP은 지정한 Station의 Up Stroke를 변경 \n
				  DOWN은 지정한 Station의 Down Stroke를 변경 \n
				  PITCH는 지정한 Station의 Pitch에 대한 정보를 변경 \n
				  EDGE는 지정한 Station의 Edge Offset에 대한 정보를 변경 \n
				  MASK는 지정한 Station의 마스크 두께에 대한 정보를 변경 \n
				  SPEED는 지정한 Station의 로봇 속도에 대한 정보를 변경 \n
				  SLOT은 지정한 Station의 최대 Slot의 정보를 변경 \n
				  AWC은 지정한 Station의 AWC 사용 유무를 변경 (ON:사용, OFF:미사용)
	*/
	void SetStationTeachingData(int nStation, CString strArg);
	
	/**
	*@fn int SetRobotSpeed()
	*@brief 로봇 구동 속도를 설정
	*@date 2019/07/11
	*@param nSpeed 구동속도 (1~100%)
	*@remark \n
			 로봇 구동중에는 사용할 수 없으며, 초기값은 50%로 설정됨
	*/
	int SetRobotSpeed(int nSpeed);
	
	/**
	*@fn void ResetError()
	*@brief 로봇의 에러를 리셋
	*@date 2019/07/11
	*/
	void ResetError();

	/**
	*@fn int GetErrorCode(CString sCode)
	*@brief Error Code 변환
	*@date 2019/12/20
	*@param Code
	*@return Error Code
	*/
	int GetErrorCode(CString sCode);
};

