#include "stdafx.h"
#include "CACSMC4UCtrl.h"


CACSMC4UCtrl::CACSMC4UCtrl()
{
	m_hComm = ACSC_INVALID;
	m_strIPaddress = "127.0.0.1"; //default address
	m_bConnect = FALSE;
	m_bMovingAvailable = FALSE;
	m_nErrorCode = 0;

	m_stXaxis.bFault = FALSE;
	m_stXaxis.bMoving = FALSE;
	m_stXaxis.bZeroReturn = FALSE;
	m_stXaxis.strStatus = "";
	m_stYaxis.bFault = FALSE;
	m_stYaxis.bMoving = FALSE;
	m_stYaxis.bZeroReturn = FALSE;
	m_stYaxis.strStatus = "";
	m_stStage.bFault = FALSE;
	m_stStage.bMoving = FALSE;
	m_stStage.bZeroReturn = FALSE;
	m_stStage.strStatus = "";
	m_bLaserFeedbackFlag = FALSE;
	m_bLaserSwichingModeFlag = FALSE;
}

CACSMC4UCtrl::~CACSMC4UCtrl()
{
	if (m_bConnect == TRUE)
	{
		DisconnectComm();
	}
}

BOOL CACSMC4UCtrl::ConnectACSController(int CommunicationType, char *Ip, int nPort)
{
	m_hComm = ACSC_INVALID;
	m_bConnect = FALSE;
	m_bMovingAvailable = FALSE;
	switch (CommunicationType)
	{
	case ETHERNET:
		//m_hComm = acsc_OpenCommEthernet((LPSTR)LPCTSTR(m_strIPaddress), ACSC_SOCKET_STREAM_PORT/*ACSC_SOCKET_DGRAM_PORT*/);
		m_hComm = acsc_OpenCommEthernet(Ip, ACSC_SOCKET_STREAM_PORT/*ACSC_SOCKET_DGRAM_PORT*/);
		if (m_hComm == ACSC_INVALID)
		{
			m_nErrorCode = acsc_GetLastError();
		}
		m_strIPaddress = Ip;
		break;
	case SERIAL:
		break;
	case SIMULATOR:
		//m_hComm = acsc_OpenCommDirect();
		m_hComm = acsc_OpenCommSimulator();
		if (m_hComm == ACSC_INVALID)
		{
			m_nErrorCode = acsc_GetLastError();
		}
		break;
	}

	if (m_hComm != ACSC_INVALID)
	{
		m_bConnect = TRUE;
		m_bMovingAvailable = TRUE;
		acsc_OpenHistoryBuffer(m_hComm, 100000);
		m_stStage.bZeroReturn = TRUE;			//추후 프로그램을 껏다 켜도 HOMMING 여부를 판단하는 방법을 찾자
	}
	return m_bConnect;
}

int CACSMC4UCtrl::DisconnectComm()
{
	acsc_CloseHistoryBuffer(m_hComm);
	acsc_ReleaseComm(m_hComm);
	acsc_CloseComm(m_hComm);
	m_hComm = ACSC_INVALID;
	m_bConnect = FALSE;
	m_bMovingAvailable = FALSE;
	return 0;
}

int CACSMC4UCtrl::SetLaserSwitchingFunction(BOOL bOnOff)
{
	if (!m_bConnect)
		return !XY_NAVISTAGE_OK;

	if (bOnOff)
	{
		if (acsc_WaitProgramEnd(m_hComm, CACULATE_ENCODER_LASER_FEEDBACK_BUUFER, 100))
		{
			if (!acsc_RunBuffer(m_hComm, CACULATE_ENCODER_LASER_FEEDBACK_BUUFER, NULL, NULL))
			{
				ErrorsHandler();
				return !XY_NAVISTAGE_OK;
			}
		}

		if (acsc_WaitProgramEnd(m_hComm, X_FEECBACK_SWITCHING_MODE_BUUFER, 100))
		{
			if (!acsc_RunBuffer(m_hComm, X_FEECBACK_SWITCHING_MODE_BUUFER, NULL, NULL))
			{
				ErrorsHandler();
				return !XY_NAVISTAGE_OK;
			}
		}
		if (acsc_WaitProgramEnd(m_hComm, Y_FEECBACK_SWITCHING_MODE_BUUFER, 100))
		{
			if (!acsc_RunBuffer(m_hComm, Y_FEECBACK_SWITCHING_MODE_BUUFER, NULL, NULL))
			{
				ErrorsHandler();
				return !XY_NAVISTAGE_OK;
			}
		}
		//if (acsc_WaitProgramEnd(m_hComm, FEECBACK_SWITCHING_OFF_BUUFER, 100))
		//{
		//	if (!acsc_RunBuffer(m_hComm, FEECBACK_SWITCHING_OFF_BUUFER, NULL, NULL))
		//	{
		//		ErrorsHandler();
		//		return !XY_NAVISTAGE_OK;
		//	}
		//}
	}
	else
	{
		//if (!acsc_StopBuffer(m_hComm, FEECBACK_SWITCHING_OFF_BUUFER, NULL))
		//{
		//	ErrorsHandler();
		//	return !XY_NAVISTAGE_OK;
		//}
		//if (!acsc_StopBuffer(m_hComm, FEECBACK_SWITCHING_ON_BUUFER, NULL))
		//{
		//	ErrorsHandler();
		//	return !XY_NAVISTAGE_OK;
		//}
		if (!acsc_StopBuffer(m_hComm, Y_FEECBACK_SWITCHING_MODE_BUUFER, NULL))
		{
			ErrorsHandler();
			return !XY_NAVISTAGE_OK;
		}
		if (!acsc_StopBuffer(m_hComm, X_FEECBACK_SWITCHING_MODE_BUUFER, NULL))
		{
			ErrorsHandler();
			return !XY_NAVISTAGE_OK;
		}
		//if (acsc_WaitProgramEnd(m_hComm, FEECBACK_SWITCHING_OFF_BUUFER, 100))
		//{
		//	if (!acsc_RunBuffer(m_hComm, FEECBACK_SWITCHING_OFF_BUUFER, NULL, NULL))
		//	{
		//		ErrorsHandler();
		//		return !XY_NAVISTAGE_OK;
		//	}
		//}
	}

	return XY_NAVISTAGE_OK;
}

//BOOL CACSMC4UCtrl::IsLaserSwichingMode()
//{
//	//10,20,21 버퍼 중 하나라도 실행되어있지 않으면 Laser-Scale Switching 불가.
//	//29,30번은 실행 이력이 있어야함
//
//	if (acsc_WaitProgramEnd(m_hComm, CACULATE_ENCODER_LASER_FEEDBACK_BUUFER, 100))
//	{
//		m_bLaserSwichingModeFlag = FALSE;
//		return m_bLaserSwichingModeFlag;
//	}
//	if (acsc_WaitProgramEnd(m_hComm, Y_FEECBACK_SWITCHING_MODE_BUUFER, 100))
//	{
//		m_bLaserSwichingModeFlag = FALSE;
//		return m_bLaserSwichingModeFlag;
//	}
//	if (acsc_WaitProgramEnd(m_hComm, X_FEECBACK_SWITCHING_MODE_BUUFER, 100))
//	{
//		m_bLaserSwichingModeFlag = FALSE;
//		return m_bLaserSwichingModeFlag;
//	}
//
//	m_bLaserSwichingModeFlag = TRUE;
//	return m_bLaserSwichingModeFlag;
//}

BOOL CACSMC4UCtrl::IsLaserSwichingModeNew()
{
	//10,20,21 버퍼 중 하나라도 실행되어있지 않으면 Laser-Scale Switching 불가.
	//Homming이되고 (1,2 번 버퍼)되고 29,30번은(Servo Boost) 실행 이력이 있어야함

	//int IsHomeDone[2];
	//	GetGlobalIntVariableArray("IsHomeDone", 0, 1, IsHomeDone);

	//if (IsHomeDone[0] != TRUE || IsHomeDone[1] != TRUE)
	//{
	//	m_bLaserSwichingModeFlag = FALSE;
	//	return m_bLaserSwichingModeFlag;
	//}

	int IsEnableServoBoost[2];
	GetGlobalIntVariableArray("SERVO_BOOST_MODE", 0, 1, IsEnableServoBoost);

	if (IsEnableServoBoost[0] != TRUE || IsEnableServoBoost[1] != TRUE)
	{
		m_bLaserSwichingModeFlag = FALSE;
		return m_bLaserSwichingModeFlag;
	}
	if (!IsBufferRun(CACULATE_ENCODER_LASER_FEEDBACK_BUUFER))
	{
		m_bLaserSwichingModeFlag = FALSE;
		return m_bLaserSwichingModeFlag;
	}
	if (!IsBufferRun(Y_FEECBACK_SWITCHING_MODE_BUUFER))
	{
		m_bLaserSwichingModeFlag = FALSE;
		return m_bLaserSwichingModeFlag;
	}
	if (!IsBufferRun(X_FEECBACK_SWITCHING_MODE_BUUFER))
	{
		m_bLaserSwichingModeFlag = FALSE;
		return m_bLaserSwichingModeFlag;
	}
	m_bLaserSwichingModeFlag = TRUE;
	return m_bLaserSwichingModeFlag;

}

int CACSMC4UCtrl::SetEncoderMode()
{
	int nRet = -1;
	nRet = RunBuffer(ENCODER_MODE_BUFFER);
	return nRet;
}


int CACSMC4UCtrl::SetEncoderModeWithDriftReset()
{
	int nRet = -1;
	nRet = RunBuffer(ENCODER_MODE_DRIFT_RESET_BUFFER);
	return nRet;
}


int CACSMC4UCtrl::SetLaserMode()
{
	int nRet = -1;
	nRet = RunBuffer(LASER_MODE_BUFFER);
	return nRet;
}


int CACSMC4UCtrl::SetLaserModeWithDriftCorrection()
{
	int nRet = -1;
	nRet = RunBuffer(LASER_MODE_DRIFT_CORRECTION_BUFFER);
	return nRet;
}

//int CACSMC4UCtrl::SetFeedbackType(int nFeedbackType)
//{
//	if (!m_bConnect)
//		return !XY_NAVISTAGE_OK;
//
//	if (nFeedbackType == FEEDBACK_LASER)
//	{
//		if (!acsc_StopBuffer(m_hComm, FEECBACK_SWITCHING_OFF_BUUFER, NULL))
//		{
//			ErrorsHandler();
//			return !XY_NAVISTAGE_OK;
//		}
//		if (!acsc_RunBuffer(m_hComm, FEECBACK_SWITCHING_ON_BUUFER, NULL, NULL))
//		{
//			ErrorsHandler();
//			return !XY_NAVISTAGE_OK;
//		}
//	}
//	else if (nFeedbackType == FEEDBACK_ENCODER)
//	{
//		if (!acsc_StopBuffer(m_hComm, FEECBACK_SWITCHING_ON_BUUFER, NULL))
//		{
//			ErrorsHandler();
//			return !XY_NAVISTAGE_OK;
//		}
//		if (!acsc_RunBuffer(m_hComm, FEECBACK_SWITCHING_OFF_BUUFER, NULL, NULL))
//		{
//			ErrorsHandler();
//			return !XY_NAVISTAGE_OK;
//		}
//	}
//
//	return XY_NAVISTAGE_OK;
//
//}

//
//int CACSMC4UCtrl::SetFeedbackTypeNew(int nFeedbackType, int DriftCorrectionMode)
//{
//	int nRet = -1;
//
//	if (!m_bConnect) { nRet = !XY_NAVISTAGE_OK; return nRet; }
//
//	if (nFeedbackType == FEEDBACK_LASER)
//	{
//		if (DriftCorrectionMode == WITHOUT_DRIFT_CORRECTION)
//		{
//			nRet = SetLaserMode();
//		}		
//		else if (DriftCorrectionMode == DRIFT_CORRECTION)
//		{
//			nRet = SetLaserModeWithDriftCorrection();
//		}
//
//	}
//	else if (nFeedbackType == FEEDBACK_ENCODER)
//	{
//		nRet = SetEncoderMode();
//	}
//
//	return nRet;
//}


//BOOL CACSMC4UCtrl::IsLaserFeedback()
//{
//	if (!acsc_WaitProgramEnd(m_hComm, FEECBACK_SWITCHING_ON_BUUFER, 100))	//24번 버퍼가 실행중이라고 Laser Feedback 상태라는 보장은 없음. 추후 더 확실한 보완책 필요.
//	{
//		m_bLaserFeedbackFlag = TRUE;
//		return TRUE;
//	}
//	m_bLaserFeedbackFlag = FALSE;
//	return FALSE;
//}

BOOL CACSMC4UCtrl::IsLaserFeedbackNew()
{
	//10번(CACULATE_ENCODER_LASER_FEEDBACK_BUUFER) 버퍼가 실행중이여야 Mode Update 됨	
	int nRet = -1; // -1: error, 0: Encoder 1: Laser

	// 버퍼 실행 확인 
	if (IsBufferRun(CACULATE_ENCODER_LASER_FEEDBACK_BUUFER))
	{
		int laer_mode[2]; //0:X(upper) 1:Y(lower)
		GetGlobalIntVariableArray("LASER_MODE", 0, 1, laer_mode);

		//if (laer_mode[0] == laer_mode[1] == 1)
		if (laer_mode[0] == 1 && laer_mode[1] == 1)
		{
			m_bLaserFeedbackFlag = TRUE;
			nRet = 1;
		}
		else
		{
			m_bLaserFeedbackFlag = FALSE;
			nRet = 0;
		}

	}
	else
	{
		nRet = -1;
	}

	return nRet;
}

BOOL CACSMC4UCtrl::IsBufferRun(int BufferNo)
{
	// 버퍼 실행 확인 
	int PST; //Program Buffer State 0: Compiled(#COMPILED) 1:Running(#RUN) 2:Suspended(#SUSPEND) 7:Autoroutine is running(#AUTO) 8:In HOLD State(#BUFHOLD)
	GetGlobalIntVariableArray("PST", BufferNo, BufferNo, &PST);
	BOOL IsRun = (PST & 0x02) == 0x02 ? TRUE : FALSE;

	return IsRun;
}

int CACSMC4UCtrl::Home(int axis)
{
	if (!m_bConnect)
		return !XY_NAVISTAGE_OK;

	switch (axis) {
	case STAGE_ALL_AXIS:
		m_stStage.bZeroReturn = FALSE;
		m_stXaxis.bZeroReturn = FALSE;
		m_stYaxis.bZeroReturn = FALSE;
		if (!acsc_StopBuffer(m_hComm, STAGE_X_AXIS, NULL))
		{
			ErrorsHandler();
			return !XY_NAVISTAGE_OK;
		}
		if (!acsc_StopBuffer(m_hComm, STAGE_Y_AXIS, NULL))
		{
			ErrorsHandler();
			return !XY_NAVISTAGE_OK;
		}
		break;
	case STAGE_X_AXIS:
		m_stStage.bZeroReturn = FALSE;
		m_stXaxis.bZeroReturn = FALSE;
		if (!acsc_StopBuffer(m_hComm, STAGE_X_AXIS, NULL))
		{
			ErrorsHandler();
			return !XY_NAVISTAGE_OK;
		}
		break;
	case STAGE_Y_AXIS:
		m_stStage.bZeroReturn = FALSE;
		m_stYaxis.bZeroReturn = FALSE;
		if (!acsc_StopBuffer(m_hComm, STAGE_Y_AXIS, NULL))
		{
			ErrorsHandler();
			return !XY_NAVISTAGE_OK;
		}
		break;
	default:
		break;
	}

	switch (axis) {
	case STAGE_ALL_AXIS:
		if (!acsc_RunBuffer(m_hComm, STAGE_X_AXIS, NULL, NULL))
		{
			ErrorsHandler();
			return !XY_NAVISTAGE_OK;
		}
		if (!acsc_RunBuffer(m_hComm, STAGE_Y_AXIS, NULL, NULL))
		{
			ErrorsHandler();
			return !XY_NAVISTAGE_OK;
		}
		break;
	case STAGE_X_AXIS:
		if (!acsc_RunBuffer(m_hComm, STAGE_X_AXIS, NULL, NULL))
		{
			ErrorsHandler();
			return !XY_NAVISTAGE_OK;
		}
		break;
	case STAGE_Y_AXIS:
		if (!acsc_RunBuffer(m_hComm, STAGE_Y_AXIS, NULL, NULL))
		{
			ErrorsHandler();
			return !XY_NAVISTAGE_OK;
		}
		break;
	default:
		break;
	}

	switch (axis) {
	case STAGE_ALL_AXIS:
		if (WaitTimeUntilBufferStop(HOMMING_TIMEOUT_SEC, STAGE_X_AXIS))
		{
			return !XY_NAVISTAGE_OK;
		}
		m_stXaxis.bZeroReturn = TRUE;
		if (WaitTimeUntilBufferStop(HOMMING_TIMEOUT_SEC, STAGE_Y_AXIS))
		{
			return !XY_NAVISTAGE_OK;
		}
		m_stYaxis.bZeroReturn = TRUE;
		break;
	case STAGE_X_AXIS:
		if (WaitTimeUntilBufferStop(HOMMING_TIMEOUT_SEC, STAGE_X_AXIS))
		{
			return !XY_NAVISTAGE_OK;
		}
		m_stXaxis.bZeroReturn = TRUE;
		break;
	case STAGE_Y_AXIS:
		if (WaitTimeUntilBufferStop(HOMMING_TIMEOUT_SEC, STAGE_Y_AXIS))
		{
			return !XY_NAVISTAGE_OK;
		}
		m_stYaxis.bZeroReturn = TRUE;
		break;
	default:
		break;
	}

	m_stStage.bZeroReturn = TRUE;

	return XY_NAVISTAGE_OK;
}

void CACSMC4UCtrl::ErrorsHandler()
{
	char chAcsErrorString[256];
	int nAcsErrorCode, nAcsErrorStringNo;
	CString str;

	nAcsErrorCode = acsc_GetLastError();
	if (acsc_GetErrorString(m_hComm, nAcsErrorCode, chAcsErrorString, 255, &nAcsErrorStringNo))
	{
		chAcsErrorString[nAcsErrorStringNo] = '\0';
		str.Format("Error:%d,%s", nAcsErrorCode, chAcsErrorString);
		//20211208 jkseo, Navigation 연결 끊김 확인을 위한 로그 추가
		SaveLogFile("NavigationStageError", str);
		AfxMessageBox(str, MB_ICONERROR);
		DisconnectComm();
		m_stStage.strStatus = str;
		m_stStage.bFault = TRUE;
	}
	//에러내용 저장하자

	if (nAcsErrorCode == 197)	//Communication Error 발생시 자동 연결 ? 필요한가 ?
	{
		m_bConnect = FALSE;
		ConnectACSController(ETHERNET, (LPSTR)(LPCTSTR)m_strIPaddress, ACSC_SOCKET_STREAM_PORT);
		if (m_bConnect == FALSE)
		{
			m_stStage.strStatus.Format("Stage와 통신이 끊겼습니다!");
			m_stStage.bFault = TRUE;
		}
	}
}

int CACSMC4UCtrl::GetAmpFault(int axis)
{
	if (!m_bConnect)
		return !XY_NAVISTAGE_OK;

	int Fault;
	if (!acsc_GetFault(m_hComm, axis, &Fault, NULL))
	{
		ErrorsHandler();
		return !XY_NAVISTAGE_OK;
	}
	if (Fault)
		return !XY_NAVISTAGE_OK;
	return XY_NAVISTAGE_OK;
}

int CACSMC4UCtrl::ClearAmpFault(int axis)
{
	if (!m_bConnect)
		return !XY_NAVISTAGE_OK;

	int Axes[] = { STAGE_X_AXIS,STAGE_Y_AXIS,-1 };

	switch (axis) {
	case STAGE_ALL_AXIS:
		if (!acsc_FaultClearM(m_hComm, Axes, NULL))
		{
			ErrorsHandler();
			return !XY_NAVISTAGE_OK;
		}
		break;
	default:
		if (!acsc_FaultClear(m_hComm, axis, NULL))
		{
			ErrorsHandler();
			return !XY_NAVISTAGE_OK;
		}
		break;
	}
	return XY_NAVISTAGE_OK;
}

int CACSMC4UCtrl::Stop(int axis)
{
	if (!m_bConnect)
		return !XY_NAVISTAGE_OK;

	int Axes[] = { STAGE_X_AXIS,STAGE_Y_AXIS };

	switch (axis) {
	case STAGE_ALL_AXIS:
		if (!acsc_HaltM(m_hComm, Axes, NULL))
		{
			ErrorsHandler();
			return !XY_NAVISTAGE_OK;
		}
		break;
	default:
		if (!acsc_Halt(m_hComm, axis, NULL))
		{
			ErrorsHandler();
			return !XY_NAVISTAGE_OK;
		}
		break;
	}
	return XY_NAVISTAGE_OK;
}

int CACSMC4UCtrl::EStop(int axis)
{
	if (!m_bConnect)
		return !XY_NAVISTAGE_OK;

	switch (axis) {
	case STAGE_ALL_AXIS:
		if (!acsc_KillAll(m_hComm, NULL))
		{
			ErrorsHandler();
			return !XY_NAVISTAGE_OK;
		}
		break;
	default:
		if (!acsc_Kill(m_hComm, axis, NULL))
		{
			ErrorsHandler();
			return !XY_NAVISTAGE_OK;
		}
		break;
	}
	return XY_NAVISTAGE_OK;
}

int CACSMC4UCtrl::SetPosmm(int axis, double pos_mm)
{
	if (!m_bConnect)
		return 0;

	if (!acsc_SetFPosition(m_hComm, axis, pos_mm, NULL))
	{
		ErrorsHandler();
		return !XY_NAVISTAGE_OK;
	}
	return XY_NAVISTAGE_OK;
}


double CACSMC4UCtrl::GetTargetPosmm(int axis)
{
	if (!m_bConnect)
		return 0;

	double get_pos_mm = 0.0f;

	if (!acsc_GetTargetPosition(m_hComm, axis, &get_pos_mm, NULL))
	{
		ErrorsHandler();
		return !XY_NAVISTAGE_OK;
	}

	return get_pos_mm;
}

double CACSMC4UCtrl::GetPosmm(int axis)
{
	if (!m_bConnect)
		return 0;

	double get_pos_mm = 0.0f;

	if (!acsc_GetFPosition(m_hComm, axis, &get_pos_mm, NULL))
	{
		ErrorsHandler();
		return !XY_NAVISTAGE_OK;
	}

	return get_pos_mm;
}



double CACSMC4UCtrl::GetGlobalRealVariable(char* varName)
{
	if (!m_bConnect)
		return 0;

	double get_value = 0.0f;

	if (!acsc_ReadReal(m_hComm, ACSC_NONE, varName, ACSC_NONE, ACSC_NONE, ACSC_NONE, ACSC_NONE, &get_value, NULL))
	{
		ErrorsHandler();
		return !XY_NAVISTAGE_OK;
	}
	return get_value;
}


int CACSMC4UCtrl::GetGlobalRealVariableArray(char* varName, int from, int to, double* values)
{
	if (!m_bConnect)
		return 0;

	if (!acsc_ReadReal(m_hComm, ACSC_NONE, varName, from, to, ACSC_NONE, ACSC_NONE, values, NULL))
	{
		ErrorsHandler();
		return !XY_NAVISTAGE_OK;
	}

	return XY_NAVISTAGE_OK;
}


int CACSMC4UCtrl::GetGlobalIntVariable(char* varName)
{
	if (!m_bConnect)
		return 0;

	int get_value = 0.0f;

	if (!acsc_ReadInteger(m_hComm, ACSC_NONE, varName, ACSC_NONE, ACSC_NONE, ACSC_NONE, ACSC_NONE, &get_value, NULL))
	{
		ErrorsHandler();
		return !XY_NAVISTAGE_OK;
	}
	return get_value;
}


int CACSMC4UCtrl::GetGlobalIntVariableArray(char* varName, int from, int to, int* values)
{
	if (!m_bConnect)
		return 0;

	if (!acsc_ReadInteger(m_hComm, ACSC_NONE, varName, from, to, ACSC_NONE, ACSC_NONE, values, NULL))
	{
		ErrorsHandler();
		return !XY_NAVISTAGE_OK;
	}

	return XY_NAVISTAGE_OK;
}

int CACSMC4UCtrl::SetGlobalIntVariable(char* varName, int setValue)
{
	if (!m_bConnect)
		return !XY_NAVISTAGE_OK;;

	if (!acsc_WriteInteger(m_hComm, ACSC_NONE, varName, ACSC_NONE, ACSC_NONE, ACSC_NONE, ACSC_NONE, &setValue, NULL))
	{
		ErrorsHandler();
		return !XY_NAVISTAGE_OK;
	}

	return XY_NAVISTAGE_OK;
}

// 동작 확인 필요 ihlee 20201012
int CACSMC4UCtrl::SetGlobalRealVariable(char* varName, double setValue)
{
	if (!m_bConnect)
		return !XY_NAVISTAGE_OK;;

	if (!acsc_WriteReal(m_hComm, ACSC_NONE, varName, ACSC_NONE, ACSC_NONE, ACSC_NONE, ACSC_NONE, &setValue, NULL))
	{
		ErrorsHandler();
		return !XY_NAVISTAGE_OK;
	}

	return XY_NAVISTAGE_OK;
}


int CACSMC4UCtrl::SetAmpEnable(int axis, BOOL value)
{
	if (!m_bConnect)
		return !XY_NAVISTAGE_OK;

	int Axes[] = { STAGE_X_AXIS,STAGE_Y_AXIS };

	switch (axis) {
	case STAGE_ALL_AXIS:
		if (value == TRUE)
		{
			if (!acsc_EnableM(m_hComm, Axes, NULL))
			{
				ErrorsHandler();
				return !XY_NAVISTAGE_OK;
			}
		}
		else
		{
			if (!acsc_DisableAll(m_hComm, NULL))
			{
				ErrorsHandler();
				return !XY_NAVISTAGE_OK;
			}
		}
		break;
	default:
		if (value == TRUE)
		{
			if (!acsc_Enable(m_hComm, axis, NULL))
			{
				ErrorsHandler();
				return !XY_NAVISTAGE_OK;
			}
		}
		else
		{
			if (!acsc_Disable(m_hComm, axis, NULL))
			{
				ErrorsHandler();
				return !XY_NAVISTAGE_OK;
			}
		}
		break;
	}
	return XY_NAVISTAGE_OK;
}

BOOL CACSMC4UCtrl::GetAmpEnable(int axis)
{
	if (!m_bConnect)
		return !XY_NAVISTAGE_OK;

	int state = 0;

	if (!acsc_GetMotorState(m_hComm, axis, &state, NULL))
	{
		ErrorsHandler();
		return !XY_NAVISTAGE_OK;
	}

	if (state & ACSC_MST_ENABLE)
		return TRUE;
	else
		return FALSE;
}

int CACSMC4UCtrl::GetMotorStatus(int axis, int* state)
{
	if (!m_bConnect)
		return !XY_NAVISTAGE_OK;

	if (!acsc_GetMotorState(m_hComm, axis, state, NULL))
	{
		ErrorsHandler();
		return !XY_NAVISTAGE_OK;
	}
	return XY_NAVISTAGE_OK;
}

int CACSMC4UCtrl::GetFaultStatus(int axis, int* fault)
{
	if (!m_bConnect)
		return !XY_NAVISTAGE_OK;

	if (!acsc_GetFault(m_hComm, axis, fault, NULL))
	{
		ErrorsHandler();
		return !XY_NAVISTAGE_OK;
	}
	return XY_NAVISTAGE_OK;
}

int CACSMC4UCtrl::GetDigitalInput(int axis, int* value)
{
	if (!m_bConnect)
		return !XY_NAVISTAGE_OK;

	switch (axis) {
	case STAGE_X_AXIS:
		if (!acsc_GetInput(m_hComm, 0, 0, value, NULL))
		{
			ErrorsHandler();
			return !XY_NAVISTAGE_OK;
		}
		//if (!acsc_GetInputPort(m_hComm, 0, value, NULL))
		//{
		//	ErrorsHandler();
		//	return !XY_NAVISTAGE_OK;
		//}
		break;
	case STAGE_Y_AXIS:
		if (!acsc_GetInput(m_hComm, 0, 1, value, NULL))
		{
			ErrorsHandler();
			return !XY_NAVISTAGE_OK;
		}
		break;
	}
	return XY_NAVISTAGE_OK;
}

int CACSMC4UCtrl::GetAnalogInput(int axis)
{
	if (!m_bConnect)
		return !XY_NAVISTAGE_OK;

	int Value;
	switch (axis) {
	case STAGE_X_AXIS:
		if (!acsc_GetAnalogInput(m_hComm, X_LOADINGPOS_SENSOR, &Value, NULL))
		{
			ErrorsHandler();
			return !XY_NAVISTAGE_OK;
		}
		break;
	case STAGE_Y_AXIS:
		if (!acsc_GetAnalogInput(m_hComm, Y_LOADINGPOS_SENSOR, &Value, NULL))
		{
			ErrorsHandler();
			return !XY_NAVISTAGE_OK;
		}
		break;
	}
	return Value;
}

int CACSMC4UCtrl::InPosition(int axis, int MaxTime, double pos_mm)
{
	clock_t t = clock();
	while (MaxTime - ((clock() - t) / CLOCKS_PER_SEC) > 0)
	{
		int State;
		if (!acsc_GetMotorState(m_hComm, axis, &State, NULL))
			ErrorsHandler();
		if (State & ACSC_MST_INPOS)
		{
			double tarpos = GetPosmm(axis);
			if (fabs(tarpos - pos_mm) > 0.5)	//
			{
				return !XY_NAVISTAGE_OK;
			}

			return XY_NAVISTAGE_OK;
		}
		//ProcessMessages();
		WaitSec(1);
	}
	return !XY_NAVISTAGE_OK;
}

int CACSMC4UCtrl::WaitInPosition(int axis, double pos_mm)
{
	if (InPosition(axis, MOVING_TIMEOUT_SEC, pos_mm) != XY_NAVISTAGE_OK)
		return !XY_NAVISTAGE_OK;
	return XY_NAVISTAGE_OK;
}

int CACSMC4UCtrl::MoveAbsoluteXY_UntilInposition(double Xposition, double Yposition)
{
	if (m_bConnect == FALSE || m_stStage.bZeroReturn == FALSE || m_bMovingAvailable == FALSE)
		return !XY_NAVISTAGE_OK;

	if (MoveAbsolute(STAGE_X_AXIS, Xposition) != XY_NAVISTAGE_OK)
		return !XY_NAVISTAGE_OK;
	if (MoveAbsolute(STAGE_Y_AXIS, Yposition) != XY_NAVISTAGE_OK)
		return !XY_NAVISTAGE_OK;
	if (WaitInPosition(STAGE_X_AXIS, Xposition) != XY_NAVISTAGE_OK)
		return !XY_NAVISTAGE_OK;
	if (WaitInPosition(STAGE_Y_AXIS, Yposition) != XY_NAVISTAGE_OK)
		return !XY_NAVISTAGE_OK;

	return XY_NAVISTAGE_OK;
}

int CACSMC4UCtrl::MoveAbsoluteXY_OnTheFly(double Xposition, double Yposition)
{
	if (m_bConnect == FALSE || m_stStage.bZeroReturn == FALSE || m_bMovingAvailable == FALSE)
		return !XY_NAVISTAGE_OK;

	if (MoveAbsolute(STAGE_X_AXIS, Xposition) != XY_NAVISTAGE_OK) return !XY_NAVISTAGE_OK;
	if (MoveAbsolute(STAGE_Y_AXIS, Yposition) != XY_NAVISTAGE_OK) return !XY_NAVISTAGE_OK;
	return XY_NAVISTAGE_OK;
}

int CACSMC4UCtrl::MoveRelativeOnTheFly(int axis, double posmm)
{
	if (m_bConnect == FALSE || m_stStage.bZeroReturn == FALSE || m_bMovingAvailable == FALSE)
		return !XY_NAVISTAGE_OK;

	if (GetAmpFault(axis) != XY_NAVISTAGE_OK)
	{
		int ret = ClearAmpFault(axis);
		if (ret != XY_NAVISTAGE_OK)
			return !XY_NAVISTAGE_OK;
	}
	if (GetAmpEnable(axis) == FALSE)
	{
		CString str;
		int ret;
		ret = SetAmpEnable(axis, TRUE);
		if (ret != XY_NAVISTAGE_OK)
		{
			return !XY_NAVISTAGE_OK;
		}
	}

	double			get_pos_mm = 0.0f, rel_pos_mm = 0.0f;

	get_pos_mm = GetPosmm(axis);

	switch (axis) {
	case STAGE_X_AXIS:
		rel_pos_mm = posmm;
		if (((get_pos_mm + rel_pos_mm) > SOFT_LIMIT_PLUS_X))
			rel_pos_mm = SOFT_LIMIT_PLUS_X - get_pos_mm;
		else if ((get_pos_mm + rel_pos_mm) < SOFT_LIMIT_MINUS_X)
			rel_pos_mm = SOFT_LIMIT_MINUS_X - get_pos_mm;
		break;
	case STAGE_Y_AXIS:
		rel_pos_mm = posmm;
		if (((get_pos_mm + rel_pos_mm) > SOFT_LIMIT_PLUS_Y))
			rel_pos_mm = SOFT_LIMIT_PLUS_Y - get_pos_mm;
		else if ((get_pos_mm + rel_pos_mm) < SOFT_LIMIT_MINUS_Y)
			rel_pos_mm = SOFT_LIMIT_MINUS_Y - get_pos_mm;
		break;
	}

	if (!acsc_FaultClear(m_hComm, axis, NULL))
	{
		ErrorsHandler();
		return !XY_NAVISTAGE_OK;
	}

	if (GetAmpEnable(axis) == TRUE)
	{
		if (!acsc_ToPoint(m_hComm, ACSC_AMF_RELATIVE, axis, rel_pos_mm, NULL))
		{
			ErrorsHandler();
			return !XY_NAVISTAGE_OK;
		}
	}
	return XY_NAVISTAGE_OK;
}

int CACSMC4UCtrl::MoveAbsolute(int axis, double posmm)
{
	if (m_bConnect == FALSE || m_stStage.bZeroReturn == FALSE || m_bMovingAvailable == FALSE)
		return !XY_NAVISTAGE_OK;

	if (GetAmpEnable(axis) == FALSE)
	{
		CString str;
		int ret;
		ret = SetAmpEnable(axis, TRUE);
		if (ret != XY_NAVISTAGE_OK)
		{
			return !XY_NAVISTAGE_OK;
		}
	}
	if (GetAmpFault(axis) != XY_NAVISTAGE_OK)
	{
		int ret = ClearAmpFault(axis);
		if (ret != XY_NAVISTAGE_OK)
			return !XY_NAVISTAGE_OK;
	}

	double	target_position_mm;

	ClearAmpFault(axis);

	switch (axis) {
	case STAGE_X_AXIS:
		target_position_mm = posmm;
		if (target_position_mm < SOFT_LIMIT_MINUS_X)
		{
			target_position_mm = SOFT_LIMIT_MINUS_X;
			break;
		}
		else if (target_position_mm > SOFT_LIMIT_PLUS_X)
		{
			target_position_mm = SOFT_LIMIT_PLUS_X;
		}
		break;
	case STAGE_Y_AXIS:
		target_position_mm = posmm;
		if (target_position_mm < SOFT_LIMIT_MINUS_Y)
		{
			target_position_mm = SOFT_LIMIT_MINUS_Y;
			break;
		}
		else if (target_position_mm > SOFT_LIMIT_PLUS_Y)
		{
			target_position_mm = SOFT_LIMIT_PLUS_Y;
		}
		break;
	}

	if (!acsc_FaultClear(m_hComm, axis, NULL))
	{
		ErrorsHandler();
		return !XY_NAVISTAGE_OK;
	}

	if (GetAmpEnable(axis) == TRUE)
	{
		if (!acsc_ToPoint(m_hComm, 0, axis, target_position_mm, NULL))
		{
			ErrorsHandler();
			return !XY_NAVISTAGE_OK;
		}
	}

	return XY_NAVISTAGE_OK;
}

int CACSMC4UCtrl::WaitTimeUntilBufferStop(double sec, int buffer)
{
	int timeflag = FALSE;
	clock_t t = clock();
	MSG msg;
	while (sec - (((clock() - t) / CLOCKS_PER_SEC)) > 0)
	{
		if (PeekMessage(&msg, NULL, NULL, NULL, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		if (acsc_WaitProgramEnd(m_hComm, buffer, 100))
		{
			timeflag = FALSE;
			break;
		}
		else
		{
			timeflag = TRUE;
		}
		Sleep(1);
	}

	return timeflag;
}

int CACSMC4UCtrl::RunBuffer(int nBufferNo)
{
	if (!m_bConnect)
		return !XY_NAVISTAGE_OK;

	if (!acsc_StopBuffer(m_hComm, nBufferNo, NULL))
	{
		ErrorsHandler();
		return !XY_NAVISTAGE_OK;
	}
	if (!acsc_RunBuffer(m_hComm, nBufferNo, NULL, NULL))
	{
		ErrorsHandler();
		return !XY_NAVISTAGE_OK;
	}

	return XY_NAVISTAGE_OK;
}


int CACSMC4UCtrl::StopBuffer(int nBufferNo)
{
	if (!m_bConnect)
		return !XY_NAVISTAGE_OK;

	if (!acsc_StopBuffer(m_hComm, nBufferNo, NULL))
	{
		ErrorsHandler();
		return !XY_NAVISTAGE_OK;
	}

	return XY_NAVISTAGE_OK;
}

int CACSMC4UCtrl::Set_RPosition(int axis, double pos)
{
	if (!acsc_SetRPosition(m_hComm, axis, pos, NULL))
	{
		ErrorsHandler();
		return !XY_NAVISTAGE_OK;
	}
	return XY_NAVISTAGE_OK;
}

int CACSMC4UCtrl::Set_SAFINI_SAFETY_LL_OFF(int axis)
{
	if (!acsc_SetSafetyInputPortInv(m_hComm, axis, ACSC_SAFETY_LL, NULL))
	{
		ErrorsHandler();
		return !XY_NAVISTAGE_OK;
	}
	return XY_NAVISTAGE_OK;

}

int CACSMC4UCtrl::Set_SAFINI_SAFETY_RL_OFF(int axis)
{
	if (!acsc_SetSafetyInputPortInv(m_hComm, axis, ACSC_SAFETY_RL, NULL))
	{
		ErrorsHandler();
		return !XY_NAVISTAGE_OK;
	}
	return XY_NAVISTAGE_OK;

}

int CACSMC4UCtrl::Set_Brushlee_motoer_not_commutated()
{
	if (!acsc_WriteInteger(m_hComm, ACSC_NONE, "SAFINI", 0, 0, ACSC_NONE, ACSC_NONE, 0, NULL))
	{
		ErrorsHandler();
		return !XY_NAVISTAGE_OK;
	}
	return XY_NAVISTAGE_OK;

}

int CACSMC4UCtrl::Set_Acsc_0_Axis_HL_Off()
{
	char* acsc_command = NULL;
	int command_size = 0;

	acsc_command = _T("SAFINI0.0=0");
	command_size = strlen(acsc_command);
	//if(!acsc_Command(m_hComm,"SAFINI0.0=0", 11,NULL))
	if (!acsc_Command(m_hComm, acsc_command, command_size, NULL))
	{
		ErrorsHandler();
		return !XY_NAVISTAGE_OK;
	}

	acsc_command = _T("SAFINI0.1=0");
	command_size = strlen(acsc_command);
	//if (!acsc_Command(m_hComm, "SAFINI0.1=0", 11, NULL))
	if (!acsc_Command(m_hComm, acsc_command, command_size, NULL))
	{
		ErrorsHandler();
		return !XY_NAVISTAGE_OK;
	}
	return XY_NAVISTAGE_OK;
}

int CACSMC4UCtrl::Set_Acsc_1_Axis_HL_Off()
{
	char* acsc_command = NULL;
	int command_size = 0;


	acsc_command = _T("SAFINI1.0=0");
	command_size = strlen(acsc_command);
	if (!acsc_Command(m_hComm, acsc_command, command_size, NULL))
	{
		ErrorsHandler();
		return !XY_NAVISTAGE_OK;
	}

	acsc_command = _T("SAFINI1.1=0");
	command_size = strlen(acsc_command);
	if (!acsc_Command(m_hComm, acsc_command, command_size, NULL))
	{
		ErrorsHandler();
		return !XY_NAVISTAGE_OK;
	}
	return XY_NAVISTAGE_OK;
}

int CACSMC4UCtrl::Set_Acsc_0_Axis_Brushless_Motor_Off()
{
	char* acsc_command = _T("MFLAGS0.8=0");
	int command_size = strlen(acsc_command);

	//if (!acsc_Command(m_hComm, "MFLAGS0.8=0", 11,  NULL))
	if (!acsc_Command(m_hComm, acsc_command, command_size, NULL))
	{
		ErrorsHandler();
		return !XY_NAVISTAGE_OK;
	}
	return XY_NAVISTAGE_OK;
}

int CACSMC4UCtrl::Set_Acsc_1_Axis_Brushless_Motor_Off()
{
	char* acsc_command = _T("MFLAGS1.8=0");
	int command_size = strlen(acsc_command);

	//if (!acsc_Command(m_hComm, "MFLAGS1.8=0",11,  NULL))
	if (!acsc_Command(m_hComm, acsc_command, command_size, NULL))
	{
		ErrorsHandler();
		return !XY_NAVISTAGE_OK;
	}
	return XY_NAVISTAGE_OK;
}

int CACSMC4UCtrl::Get_Connection_info()
{
	ACSC_CONNECTION_INFO ConnectionInfo;

	if (!acsc_GetConnectionInfo(m_hComm, &ConnectionInfo))
	{
		ErrorsHandler();
		m_bConnect = FALSE;
		return !XY_NAVISTAGE_OK;
	}
	return XY_NAVISTAGE_OK;
}

int CACSMC4UCtrl::GetFeedbackVelocity(int axis, double &Fvelocity)
{
	if (!acsc_GetFVelocity(m_hComm, axis, &Fvelocity, NULL))
	{
		ErrorsHandler();
		m_bConnect = FALSE;
		return !XY_NAVISTAGE_OK;
	}
	return XY_NAVISTAGE_OK;

}


int CACSMC4UCtrl::SetVelocity(int axis, double velocity)
{
	if (!acsc_SetVelocity(m_hComm, axis, velocity, NULL))
	{
		ErrorsHandler();
		m_bConnect = FALSE;
		return !XY_NAVISTAGE_OK;
	}
	return XY_NAVISTAGE_OK;

}


int CACSMC4UCtrl::GetVelocity(int axis, double &velocity)
{
	if (!acsc_GetVelocity(m_hComm, axis, &velocity, NULL))
	{
		ErrorsHandler();
		m_bConnect = FALSE;
		return !XY_NAVISTAGE_OK;
	}
	return XY_NAVISTAGE_OK;

}


