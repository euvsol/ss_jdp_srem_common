/**
 * Dalsa Camera Control Class
 *
 * Copyright 2019 by E-SOL, Inc.,
 *
 **/

#pragma once

class AFX_EXT_CLASS CDalsaCameraCtrl : public CECommon
{
public:
	CDalsaCameraCtrl();
	~CDalsaCameraCtrl();
};

