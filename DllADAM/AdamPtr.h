/**
 * ADAM(Advanced Data Aquisition Module) Control Class
 *
 * Copyright 2019 by E-SOL, Inc.,
 *
 * ADAM header file define by YDKIM form 2019.07.11
 *
 **/
#pragma once

#ifdef _DEBUG
#define _DEBUG_WAS_DEFINED 1
#undef _DEBUG
#endif

#include <Python.h>
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <numpy\arrayobject.h>
#include <numpy\npy_3kcompat.h>

#ifdef _DEBUG_WAS_DEFINED
#define _DEBUG
#endif

#include <thread>
#include <vector>
#include "CMemoryPool.h"
using namespace std;
using std::thread;



const int NumberOfPacket = 1000;
const int HeadSize = 6;

const int  PacketSize = 68;
const int  ReceiveBufferSize = NumberOfPacket * PacketSize + HeadSize;

//const int  MaxPointMeasureMemoryNumber = 5; 			// 스캔 횟수별로 저장하기 위한 메모리 공간 확모  
const int  MaxScanMemoryNumber = 8;			// 스캔 횟수별로 저장하기 위한 메모리 공간 확모  
//const int  MaxScanNumber = 100;	// 스캔 횟수별 최대 쓰레드 ==> 쓰레드 pool 개념으로 변경 필요.. 
//const int  AcquisitionTimeOutMs = 6000000;    //테스트로 크게 잡았음(ihlee) ==> 500000 (8.3분에서..100분으로 증가시킴)

const int  KindsOfAdamData = 9;
const int  NumI0Data = 5;
const int  KindsOfData = KindsOfAdamData + NumI0Data;	//I0_normalize 한 데이터, I0 필터링한것 추가

const int  PacketNumber = 0;
const int  Em1024X = 1;
const int  Em1024Y = 2;
const int  Detector1 = 3;
const int  Detector2 = 4;
const int  Detector3 = 5;
const int  Detector4 = 6;
const int  ZrI0 = 7;	
const int  BsI0 = 8;	

//const int  I0Index = 9; //RawImage는 라인별로 잘려져있음(packetnumber로 대체 해도 될듯?), ihlee 삭제예정
//const int  I0_Detector = Detector2; //I0용도로 사용하는 Detector  ihlee 삭제예정

const int  UniDirection = 1;
const int  BiDirection = 2;


class AFX_EXT_CLASS AdamPtr : public CEthernetCom, public CADAMAlgorithm
{
public:
	AdamPtr();
	~AdamPtr();

	virtual	int				SendData(char *lParam, int nTimeOut = 0, int nRetryCnt = 3);

	int SendData(int nSize, char *lParam, int nTimeOut = 0, int nRetryCnt = 3);
	int ReceivedData_UniD(char *lParam);	//NPacket에서 Packet을 잘라내서 이 함수로 보낸다. by smchoi
	int ReceivedData_BiD(char *lParam);		// Bi-direction 스캔 기능 추가	

	int m_nFovDataCntold;
	int ReceivedData_Average(char *lParam);
	int ReceivedData_After(char *lParam, int num_packet);

	char** m_chReceivedbuf = NULL;		//NPacket에서 Packet을 잘라서 저장하는 버퍼 by smchoi	
	char* m_chReceivedPrebuf = NULL;
	/*
	* EUV 이미지 정보를 정의하는 변수 FOV and Scan grid
	*/
	//int m_nEuvImage_Old_Fov;	//nm
	int m_nEuvImage_Fov;		//nm
	int m_nEuvImage_ScanGrid;	//nm
	int m_nEuvImage_ScanNumber;

	int m_nEUVImage_InterpolationGrid;
	int m_nEUVImage_Detectorselection;

	/**
	* Description: Scan하는 FOV의 Pixel Width, Pixel Height
	*/
	int m_nRawImage_PixelWidth;
	int m_nRawImage_PixelHeight;
	/**
	* Description: Regrid 하는 FOV의 Pixel Height, Pixel Height
	*/
	int m_nReGridImage_PixelWidth;
	int m_nReGridImage_PixelHeight;

	/**
	* Description: ADAM에서 날아오는 Data 중 유효한 FOV 영역의 data만 저장하는 변수
	*/
	double** m_dRawImageForDisplay; // ICD2에서 Display용 ihlee	
	double* m_dRawImageDataFor1DRegrid;

	/**
	*  이미지만 Buffer 하기 위한 메모리 공간
	*/
	//double** m_dReGridImageForDisplay; // ICD2에서 Display용 ihlee	

	//double*** m_dAveragedImage_Data3D;
	//double*** m_dFilteredImage_Data3D;


	double*** m_dRawImage_Data3D;
	//double*** m_dRegridImage_Data3D;

	double* m_dI0_Original = NULL;
	double* m_dI0_Filtered = NULL;

	unsigned int m_nOriignaDataI0_Index = 0;


	/**
	* Description: Scan Stage정지상태에서 ADAM에서 날아오는 Raw Data를 Real Time으로 이용하기 위한 변수
	*/
	double *m_dParsedData;

	/**
	* Laser Interferometer 값이 Uni-Direction data 획득 구간이면 TRUE, 그외 구간이면 FALSE
	*/
	BOOL m_bCanGetData_AtUniDirection;				//
	BOOL m_bCanGetData_AtYaxisDirectoin;


	// Bidirection 용으로 만들어 놓은 Falg			KYD_20200630

	BOOL m_bCanGetData_AtXaxisDirectoin;

	BOOL m_bIsScanDirectionForward;
	int m_nLineNumCnt;								/*라인마다 갯수를 정해놓고 해당 라인에서 Data가 count 되었는지 확인함*/

	int m_nFovDataCnt_Bi;

	BOOL m_bIsLineScanCompleted;
	BOOL m_bIsAcquisitionStart;							//
	int m_nReverseDataCnt;

	double pos_x_old;
	double pos_x_oldold;
	double pos_x_current;

	double pos_y_old;
	double pos_y_oldold;
	double pos_y_current;

	int m_nScanType;				// 1: Uni-Direction, 2: Bi-Direction

	/*
	* ADAM 데이터를 구조체로 정의함
	*/
	//추가로 필요한 항목을 삽입 해야함
	typedef struct ADAM_Data
	{
		unsigned long m_nPacketNum;				/** packet Number */
		double m_dX_position;
		double m_dY_position;
		double m_dDetector1;
		double m_dDetector2;
		double m_dDetector3;
		double m_dDetector4;
		double m_dI0_ZR;
		double m_dI0_BS;
		unsigned long m_nPacketNum_previous;//통신 테스트용 추가 ihlee
		
	} ADAM_Data;

	ADAM_Data AdamData;

	/* 심플 Run/Stop을 위한 명령어 선언*/
	int ADAMRunStart();
	int ADAMAbort();

	/* Interpolation 조건 및  실행을 호출하기 위한 함수*/
	//int Regrid(int scanBufIndex, int scanNum, int pointNum = 0);
	//int Averaging(int scanBufIndex, int scanNum, int totalScanNum, int pointMeasureDataIndex = 0);

	//int Filtering(int pointMeasureDataIndex = 0);

	//int GetImageCapSensorPos();
	//int GetCurrentLIFPosValue();

	/* ADAM data를 parsing하기 위한 함수 선언*/
	void AdamDataMemReset(int pointMeasureDataIndex = 0, vector<int> vec_scanBufIndex = { });

	void Parsing_ADAM_memcpy(char *original);

	/* ADAM control을 위한 global variable 선언*/
	BOOL m_bIsScaningOn;
	BOOL m_bIsScaningOnPrevious;
	BOOL m_bIsFirstScan;


	BOOL m_bSaveEveryScanRawImage = FALSE;  //

	double m_dLaserX_1st;
	double m_dLaserY_1st;

	double m_dAcelDistance_X;		// 
	double m_dAcelDistance_Y;		// N 스캔 기능 구현을 위해 Y 방향의 
	double m_dScanLine_Y_sum;
	double m_dScanLine_Y_ave;


	int	m_nFovDataCnt;
	int m_nImageScanCnt;
	int m_nRemainPixelCnt;
	int m_nScanLIneDataCnt;

	int m_nInterpolationCnt;

	double m_I0Reference;
	int m_I0FilterWindowSize;
	double m_LowPassFilterSigma;

	// EUV PTR 개발코드 부분--------------------------------------------------
	double** m_dRawMeasure_Data_PTR;

	int m_nPRTAcqTime;

	int Command_ScanStart();
	int Command_ScanStop();


	//int Command_GetValueAfter();
	int Command_Run();
	int Command_Stop();
	int Command_AverageRun();
	int Command_SetAverageCount(unsigned int count);
	int Command_AverageRunTimeout();
	int Command_MPacketRun(unsigned int count);
	int Command_MPacketRunTimeout(unsigned int count);
	int Command_EM1024Reset();

	int m_nPTRDataNumADAM;

	HANDLE m_hAcquisitionEnd; // ihlee 21.01.16 SetEvent()어딘가에서 제거됨. ㅠㅠ
	//--------------------------------------------------------------------------

	//BigEndian	
	unsigned long CharToUlong(char* buf);

	/*5Khz ICD2 적용 위한 변경*/

	BOOL bSaveNormalizedImage = TRUE;

	virtual	int ReceiveData(char *lParam, int nLength);
	virtual int WaitReceiveEventThread();																						//


	BOOL m_bAbort = FALSE;

	int previousLintCountInterpolation = 0;


	int ADAM_MAX_WIDTH = 1000;
	int ADAM_MAX_HEIGHT = 1000;
	int ADAM_MAX_WIDTH_REGRID = 3000;
	int ADAM_MAX_HEIGHT_REGRID = 3000;

	int ADAM_MAX_DATA = ADAM_MAX_WIDTH * ADAM_MAX_HEIGHT;
	int ADAM_MAX_DATA_REGRID = ADAM_MAX_WIDTH_REGRID * ADAM_MAX_HEIGHT_REGRID;
	unsigned int ORIGINAL_DATA_MAX = ADAM_MAX_DATA * MaxScanMemoryNumber * 8;

	void MemoryAllocation();

	BOOL m_bIsUseInterpolation = TRUE;
	BOOL m_bIsUseHighFovInterpolation = FALSE;
	BOOL m_bIsUseHighFovInterpolationOld = FALSE;
	int m_HighFovCriteria = 4000; //4000um 이상



protected:

	BOOL m_bReceiveBytesReset = TRUE;					// 수신 버퍼 Packet 초기화 (Adam에 packet 분할 문제 대응용, ihlee)
	BOOL m_bReceivedBytesSizeTotalUpdated = FALSE;

	int	m_nReceivedBytesSizeCurrent = 0;	        // 현재까지 수신된  Packetsize
	int m_nReceivedBytesSizeTotal = 0;	            // 수신 될 PacketSize	


	HANDLE			m_hDisplayRawImage = NULL;
	BOOL m_bAverageRunAfter = FALSE;
	//HANDLE m_hAverageRun;

public:


	BOOL m_IsUse3umFOV = TRUE;
	BOOL m_IsMeasureComplete;
	BOOL m_IsResetStartPos = TRUE;

	BOOL Is_ADAM_Connected() { return m_bConnected; }
};


