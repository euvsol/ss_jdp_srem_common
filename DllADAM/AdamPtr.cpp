#pragma once

#include "stdafx.h"
#include "AdamPtr.h"
#include "math.h"
#include <time.h>
#include <windows.h>
#include "CADAMInterpolationAlgorithm.h"
//#include "CADAMAlgorithm.h"

using namespace std;

#pragma comment(lib, "winmm.lib") 

/*
 ** ADAM 통산 관련 주요 내용 수정... 2019.07.15. by YDK
*/

// ADAM control 생성자 선언
// 왜서 최대 이미지 갯수만큼 추가 메모리 버퍼를 생성했지?

AdamPtr::AdamPtr()
{
	m_nRawImage_PixelWidth = 300;
	m_nRawImage_PixelHeight = 300;

	m_nReGridImage_PixelWidth = 1000;
	m_nReGridImage_PixelHeight = 1000;

	m_strReceiveEndOfStreamSymbol = _T("\r");  // Packet에 \r 이 있어야 한 패킷으로 인식함

	// ADAM Data Initialize
	//AdamData.m_chPacketStart = "KYD";
	AdamData.m_nPacketNum = 0;
	AdamData.m_dX_position = 0.0;
	AdamData.m_dY_position = 0.0;
	AdamData.m_dDetector1 = 0.0;
	AdamData.m_dDetector2 = 0.0;
	AdamData.m_dI0_BS = 0.0;
	AdamData.m_dI0_ZR = 0.0;
	//AdamData.m_chPacketEnd = "DYK";

	// Data Initialize
	m_dLaserX_1st = 0.0f;
	m_dLaserY_1st = 0.0f;
	m_dAcelDistance_X = 0.0f;
	m_dAcelDistance_Y = 0.0f;
	m_dScanLine_Y_sum = 0.0f;
	m_dScanLine_Y_ave = 0.0f;
	m_nFovDataCnt = 0;
	m_nImageScanCnt = 0;
	m_nRemainPixelCnt = 0;
	m_nScanLIneDataCnt = 0;

	m_nInterpolationCnt = 0;

	//m_dCurrentImagePosition_Cap1 = 0.0;
	//m_dCurrentImagePosition_Cap2 = 0.0;
	//m_dCurrentImagePosition_Cap3 = 0.0;
	//m_dCurrentImagePosition_Cap4 = 0.0;

	//m_dCurrentXposition = 0.0;
	//m_dCurrentYposition = 0.0;

	// Bi-direction
	m_nScanType = UniDirection;			// Uni-direction으로 초기화

	m_nReverseDataCnt = 0;
	m_nFovDataCnt_Bi = 0;		// Bi-direction 용 Fov data count

	pos_x_current = 0;			// Bi-diriction 방향 확인 용
	pos_x_old = 0;
	pos_x_oldold = 0;
	pos_y_current = 0;
	pos_y_old = 0;
	pos_y_oldold = 0;

	// PTR
	m_nPTRDataNumADAM = 1000;

	// 기본동작에 필요한 변수 생성
	m_bCanGetData_AtUniDirection = FALSE;									// X Scan
	m_bCanGetData_AtYaxisDirectoin = FALSE;

	// Bi direction 용으로 변수 생성										// Reverse derection 포함
	m_bIsScanDirectionForward = FALSE;
	m_nLineNumCnt = 0;														// 생성자 초기화
	m_bIsLineScanCompleted = FALSE;

	m_bCanGetData_AtXaxisDirectoin = FALSE;				// bidirection용 코드_KYD

	m_bIsAcquisitionStart = FALSE;											// Bi-direction scan 시 Data를 획득할 것인지 판단하는 Flag

	m_bIsScaningOn = FALSE;
	m_bIsScaningOnPrevious = TRUE;  //프로 그램 시작시 Cap read 지연을 위해 TRUE로 설정, 동작안함

	m_bIsFirstScan = FALSE;


	// 5Khz ICD2 Thread 관련
	//m_pAcqusitionThreads = NULL;
	//m_pAcqusitionThreads.reserve(MAX_SCAN_NUMBER);

	m_hAcquisitionEnd = CreateEvent(NULL, TRUE, FALSE, NULL);


	m_hDisplayRawImage = CreateEvent(NULL, FALSE, FALSE, NULL);//자동리셋
	//ResetEvent(m_hDisplayRawImage);
	

	//m_hAverageRun = CreateEvent(NULL, TRUE, FALSE, NULL);

}

AdamPtr::~AdamPtr()
{
	int i = 0;
	if (m_dRawImageForDisplay != NULL)
	{
		for (i = 0; i < KindsOfData; i++)
			delete[] m_dRawImageForDisplay[i];

		delete[] m_dRawImageForDisplay;
	}

	//if (m_dReGridImageForDisplay != NULL)
	//{
	//	for (i = 0; i < KIND_OF_REGRIDDATA; i++)
	//		delete[] m_dReGridImageForDisplay[i];

	//	delete[] m_dReGridImageForDisplay;
	//}

	if (m_dRawImageDataFor1DRegrid != NULL)
	{
		delete[] m_dRawImageDataFor1DRegrid;
	}

	//----------------------------------------------- PTR
	if (m_dRawMeasure_Data_PTR != NULL)
	{
		for (i = 0; i < KindsOfData; i++)
			delete[] m_dRawMeasure_Data_PTR[i];

		delete[] m_dRawMeasure_Data_PTR;
	}
	//------------------------------------------------


	// Reconstructed data 소멸자..... 해당 변수를 어디다기 넣을 지는 추가로 고민이 필요함 KYD_20200119
	/*if (m_dAveragedImage_Data3D != NULL)
	{
		for (int i = 0; i < MaxPointMeasureMemoryNumber; i++)
		{
			for (int j = 0; j < KIND_OF_REGRIDDATA; j++)
			{
				delete[] m_dAveragedImage_Data3D[i][j];
			}

			delete[] m_dAveragedImage_Data3D[i];
		}
		delete[] m_dAveragedImage_Data3D;
	}*/

	//if (m_dFilteredImage_Data3D != NULL)
	//{
	//	for (int i = 0; i < MaxPointMeasureMemoryNumber; i++)
	//	{
	//		for (int j = 0; j < KIND_OF_REGRIDDATA; j++)
	//		{
	//			delete[] m_dFilteredImage_Data3D[i][j];
	//		}

	//		delete[] m_dFilteredImage_Data3D[i];
	//	}
	//	delete[] m_dFilteredImage_Data3D;
	//}


	if (m_dRawImage_Data3D != NULL)
	{
		for (int i = 0; i < MaxScanMemoryNumber; i++)
		{
			for (int j = 0; j < KindsOfData; j++)
			{
				delete[] m_dRawImage_Data3D[i][j];
			}

			delete[] m_dRawImage_Data3D[i];
		}

		delete[] m_dRawImage_Data3D;
	}

	if (m_dI0_Original != NULL)
	{
		delete[] m_dI0_Original;
	}

	if (m_dI0_Filtered != NULL)
	{
		delete[] m_dI0_Filtered;
	}

	//if (m_dRawImage_Data_forRegrid3D != NULL)
	//{
	//	for (int i = 0; i < MAX_SCAN_MEMORY_NUMBER; i++)
	//	{
	//		for (int j = 0; j < KINDS_OF_DATA; j++)
	//		{
	//			delete[] m_dRawImage_Data_forRegrid3D[i][j];
	//		}

	//		delete[] m_dRawImage_Data_forRegrid3D[i];
	//	}

	//	delete[] m_dRawImage_Data_forRegrid3D;
	//}



	//if (m_dRegridImage_Data3D != NULL)
	//{
	//	for (int i = 0; i < MaxScanMemoryNumber; i++)
	//	{
	//		for (int j = 0; j < KIND_OF_REGRIDDATA; j++)
	//		{
	//			delete[] m_dRegridImage_Data3D[i][j];
	//		}

	//		delete[] m_dRegridImage_Data3D[i];
	//	}
	//	delete[] m_dRegridImage_Data3D;
	//}

	//if (m_dRawADAMData != NULL)
	//{
	//	delete[] m_dRawADAMData;
	//}

	//if (m_chReceivebuf != NULL)
	//{
	//	for (i = 0; i < KINDS_OF_ADAMDATA; i++)
	//		delete[] m_chReceivebuf[i];

	//	delete[] m_chReceivebuf;
	//}

	delete[] m_dParsedData;

	if (m_chReceivedbuf != NULL)
	{
		for (i = 0; i < NumberOfPacket; i++)
			delete[] m_chReceivedbuf[i];

		delete[] m_chReceivedbuf;
	}

	if (m_chReceivedPrebuf != NULL)
	{
		delete[] m_chReceivedPrebuf;
	}


	

	CloseHandle(m_hDisplayRawImage);

	//CloseHandle(m_hAverageRun);


}



void AdamPtr::MemoryAllocation()
{

	ADAM_MAX_DATA = ADAM_MAX_WIDTH * ADAM_MAX_HEIGHT;
	ADAM_MAX_DATA_REGRID = ADAM_MAX_WIDTH_REGRID * ADAM_MAX_HEIGHT_REGRID;
	ORIGINAL_DATA_MAX = ADAM_MAX_DATA * MaxScanMemoryNumber * 8;

	// 이중포인터로 2차원 배열을 생성하기 위한 생성자 구현		// 데이터 종류 X 최대 픽셀갯수 의 2차원 버퍼
	m_dRawImageForDisplay = new double *[KindsOfData];								// m_dRawImage_Data 의 메모리 버퍼를 만들어서 데이터를 획득함(ADAM 이미지 데이터 갯수만큼 버퍼 생성
	for (int i = 0; i < KindsOfData; i++)
	{
		m_dRawImageForDisplay[i] = new double[ADAM_MAX_DATA];							// m_dRawImage_Data[i]에 ADAM 에서 전송되는 image data 한 개의 패킷에 저장된 데이터들이 순서대로 기록되도록 메모리 버퍼를 생성함. 생성하는 버퍼는 22개
	}

	m_dRawImageDataFor1DRegrid = new double[ADAM_MAX_DATA];

	//m_dReGridImageForDisplay = new double *[KIND_OF_REGRIDDATA];								// m_dRawImage_Data 의 메모리 버퍼를 만들어서 데이터를 획득함(ADAM 이미지 데이터 갯수만큼 버퍼 생성
	//for (int i = 0; i < KIND_OF_REGRIDDATA; i++)
	//{
	//	m_dReGridImageForDisplay[i] = new double[ADAM_MAX_DATA_REGRID];							// m_dRawImage_Data[i]에 ADAM 에서 전송되는 image data 한 개의 패킷에 저장된 데이터들이 순서대로 기록되도록 메모리 버퍼를 생성함. 생성하는 버퍼는 22개
	//}


	// PTR용 메모리 저장 버퍼
	//-----------------------------------------
	m_dRawMeasure_Data_PTR = new double *[KindsOfData];								// m_dRawImage_Data 의 메모리 버퍼를 만들어서 데이터를 획득함(ADAM 이미지 데이터 갯수만큼 버퍼 생성
	for (int i = 0; i < KindsOfData; i++)
	{
		m_dRawMeasure_Data_PTR[i] = new double[ADAM_MAX_DATA];							// m_dRawImage_Data[i]에 ADAM 에서 전송되는 image data 한 개의 패킷에 저장된 데이터들이 순서대로 기록되도록 메모리 버퍼를 생성함. 생성하는 버퍼는 22개
	}
	//-------------------------------------------

	m_dI0_Original = new double[ORIGINAL_DATA_MAX];
	m_dI0_Filtered = new double[ORIGINAL_DATA_MAX];

	m_dRawImage_Data3D = new double**[MaxScanMemoryNumber];
	for (int i = 0; i < MaxScanMemoryNumber; i++)
	{
		m_dRawImage_Data3D[i] = new double*[KindsOfData];

		for (int j = 0; j < KindsOfData; j++)
		{
			m_dRawImage_Data3D[i][j] = new double[ADAM_MAX_DATA];
		}
	}

	//m_dRegridImage_Data3D = new double**[MaxScanMemoryNumber]; //메모리 소요 많음 18GBYTE 정도
	//for (int i = 0; i < MaxScanMemoryNumber; i++)
	//{
	//	m_dRegridImage_Data3D[i] = new double*[KIND_OF_REGRIDDATA];

	//	for (int j = 0; j < KIND_OF_REGRIDDATA; j++)
	//	{
	//		m_dRegridImage_Data3D[i][j] = new double[ADAM_MAX_DATA_REGRID];
	//		memset(m_dRegridImage_Data3D[i][j], 0, sizeof(double) * ADAM_MAX_DATA_REGRID);
	//	}
	//}


	//m_dAveragedImage_Data3D = new double**[MaxPointMeasureMemoryNumber]; //메모리 소요 많음 18GBYTE 정도
	//for (int i = 0; i < MaxPointMeasureMemoryNumber; i++)
	//{
	//	m_dAveragedImage_Data3D[i] = new double*[KIND_OF_REGRIDDATA];

	//	for (int j = 0; j < KIND_OF_REGRIDDATA; j++)
	//	{
	//		m_dAveragedImage_Data3D[i][j] = new double[ADAM_MAX_DATA_REGRID];
	//		memset(m_dAveragedImage_Data3D[i][j], 0, sizeof(double) * ADAM_MAX_DATA_REGRID);
	//	}
	//}

	//m_dFilteredImage_Data3D = new double**[MaxPointMeasureMemoryNumber]; //메모리 소요 많음 18GBYTE 정도
	//for (int i = 0; i < MaxPointMeasureMemoryNumber; i++)
	//{
	//	m_dFilteredImage_Data3D[i] = new double*[KIND_OF_REGRIDDATA];

	//	for (int j = 0; j < KIND_OF_REGRIDDATA; j++)
	//	{
	//		m_dFilteredImage_Data3D[i][j] = new double[ADAM_MAX_DATA_REGRID];
	//		memset(m_dFilteredImage_Data3D[i][j], 0, sizeof(double) * ADAM_MAX_DATA_REGRID);
	//	}
	//}

	//--------------------------------------------------

	m_dParsedData = new double[KindsOfAdamData];                              //-NUM_AUGMENTED_DATA?

	m_chReceivedbuf = new char *[NumberOfPacket];								// N Packet을 붙여서 보내는 경우 이버퍼로 잘라낸다. by smchoi
	for (int i = 0; i < NumberOfPacket; i++)
	{
		m_chReceivedbuf[i] = new char[PacketSize];
	}

	m_chReceivedPrebuf = new char[ReceiveBufferSize];

	//----------------------------------------------------
	//int i = 0;
	for (int i = 0; i < KindsOfData; i++)
		memset(m_dRawImageForDisplay[i], 0, sizeof(double) * ADAM_MAX_DATA);  // std::fill() and std::fill_n()

	memset(m_dRawImageDataFor1DRegrid, 0, sizeof(double) * ADAM_MAX_DATA);  // std::fill() and std::fill_n()	

	//MemoryPoolInitialization();


	// For Elitho

	//D3Data = new double[D3_MAX];

}
// 로그 작성용 변경 내용은 추가로 확인이 필요함 KYD 20191203 ICD 1에서만 사용
int	AdamPtr::SendData(char *lParam, int nTimeOut, int nRetryCnt)
{
	int nRet = 0;
	//21.12.21 사용안하는거 같아서 삭제

	//char logsend[60];
	//char *t1 = "ADAM command", *t3 = "transfer", *t2;
	//CString test;

	//ResetEvent(m_hStateOkEvent);	//Sets the specified event object to the nonsignaled state.

	//m_strSendMsg = lParam;
	//t2 = lParam;

	//nRet = Send(lParam, nTimeOut);

	//nRet = WaitEvent(m_hStateOkEvent, nTimeOut);

	//sprintf(logsend, "%s%s%s", t1, t2, t3); // 빈 배열 test 에 여러 문자열 집어넣기
	//test = logsend;

	//if (nRet != 0)
	//	m_strSendMsg = _T("ADAM Run command transfer");

	//SaveLogFile("ADAM_Com", _T((LPSTR)(LPCTSTR)("PC -> ADAM : " + m_strSendMsg + "Transfered")));	//통신 상태 기록.
	//SaveLogFile("ADAM_Com", _T((LPSTR)(LPCTSTR)("PC -> ADAM : " + test + "test2")));	//통신 상태 기록.

	return nRet;
}



int	AdamPtr::SendData(int nSize, char *lParam, int nTimeOut, int nRetryCnt) // ICD2 로그저장  수정해야함 ihlee
{
	int nRet = 0;
	//char logsend[60];
	char *t1 = "ADAM command", *t3 = "transfer", *t2;
	//CString test;

	EnterCriticalSection(&cs);


	// Receive 초기화
	m_bReceiveBytesReset = TRUE; // Receive Packet 초기화 ihlee

	if (nTimeOut > 0)
	{
		ResetEvent(m_hStateOkEvent);	//Sets the specified event object to the nonsignaled state.
	}

	m_strSendMsg = lParam;
	t2 = lParam;

	nRet = Send(nSize, lParam, nTimeOut, nRetryCnt);

	if (nTimeOut > 0)
	{
		nRet = WaitEvent(m_hStateOkEvent, nTimeOut);
	}

	//sprintf(logsend, "%s%s%s", t1, t2, t3); // 빈 배열 test 에 여러 문자열 집어넣기
	//test = logsend;

	if (nRet != 0)
		m_strSendMsg = _T("ADAM Run command transfer");

	SaveLogFile("ADAM_Com", _T((LPSTR)(LPCTSTR)("PC -> ADAM : " + m_strSendMsg + "Transfered")));	//통신 상태 기록.
	//SaveLogFile("ADAM_Com", _T((LPSTR)(LPCTSTR)("PC -> ADAM : " + test + "test2")));	//통신 상태 기록.

	LeaveCriticalSection(&cs);

	return nRet;
}


// Laser interferometer reset을 위해서 테스트로 만든 코드
int AdamPtr::Command_EM1024Reset()
{
	int nRet = 0;
	char chBuf[10];

	chBuf[0] = 0x01;

	chBuf[1] = 0x01;
	chBuf[2] = 0x00;
	chBuf[3] = 0x00;
	chBuf[4] = 0x00;

	chBuf[5] = 0x21;
	chBuf[6] = 0x01;

	nRet = SendData(7, chBuf);

	return nRet;
}



void AdamPtr::Parsing_ADAM_memcpy(char *original)
{
	unsigned long num_packet = CharToUlong(original);

	//Little endian		
	m_dParsedData[0] = (double)num_packet;
	memcpy(m_dParsedData + 1, original + 4, sizeof(double) * (KindsOfAdamData - 1)); // PACKET_NUMBER제외 15가지 데이터, 

	//memcpy(&AdamDataNew, original, sizeof(ADAM_DataNew));	
	AdamData.m_nPacketNum = num_packet;

	AdamData.m_dX_position = m_dParsedData[1];
	AdamData.m_dY_position = m_dParsedData[2];

	AdamData.m_dDetector1 = m_dParsedData[3];
	AdamData.m_dDetector2 = m_dParsedData[4];
	AdamData.m_dDetector3 = m_dParsedData[5];
	AdamData.m_dDetector4 = m_dParsedData[6];
	
	AdamData.m_dI0_BS = m_dParsedData[7];
	AdamData.m_dI0_ZR = m_dParsedData[8];

	if (AdamData.m_nPacketNum != 0)
	{
		if (AdamData.m_nPacketNum - AdamData.m_nPacketNum_previous != 1)
		{
			CString strLog;
			strLog.Format("[Error]Packet error previous =%lu current=%lu", AdamData.m_nPacketNum_previous, AdamData.m_nPacketNum);
			SaveLogFile("AdamLog", (LPSTR)(LPCTSTR)strLog);
		}
	}
	AdamData.m_nPacketNum_previous = AdamData.m_nPacketNum;
}

void AdamPtr::AdamDataMemReset(int pointMeasureDataIndex, vector<int> vec_scanBufIndex) // for ICD2
{
	//필요한 사이즈 만큼 리셋 하는것으로 변경 필요 ihlee

	int size = vec_scanBufIndex.size();

	if (size > 0)
	{
		for (int i = 0; i < size; i++)
		{
			int index = vec_scanBufIndex.at(i);

			for (int j = 0; j < KindsOfData; j++)
			{
				memset(m_dRawImage_Data3D[index][j], 0, sizeof(double) * m_nRawImage_PixelWidth * m_nRawImage_PixelHeight); //std::fill() and std::fill_n()
			}
		}
	}
	else
	{
		for (int i = 0; i < MaxScanMemoryNumber; i++)
		{
			for (int j = 0; j < KindsOfData; j++)
			{
				memset(m_dRawImage_Data3D[i][j], 0, sizeof(double) * m_nRawImage_PixelWidth * m_nRawImage_PixelHeight); //std::fill() and std::fill_n()
			}
		}
	}


	//if (m_bIsUseInterpolation)
	//{
	//	for (int j = 0; j < KIND_OF_REGRIDDATA; j++)
	//	{
	//		memset(m_dRegridImage_Data3D[pointMeasureDataIndex][j], 0, sizeof(double) * ADAM_MAX_DATA_REGRID);
	//		//memset(m_dAveragedImage_Data3D[pointMeasureDataIndex][j], 0, sizeof(double) * ADAM_MAX_DATA_REGRID);
	//		memset(m_dFilteredImage_Data3D[pointMeasureDataIndex][j], 0, sizeof(double) * ADAM_MAX_DATA_REGRID);
	//	}
	//}
	// TEST용 삭제해야함?, Display 리셋용
	for (int i = 0; i < KindsOfData; i++)
	{
		memset(m_dRawImageForDisplay[i], 0, sizeof(double) * m_nRawImage_PixelWidth * m_nRawImage_PixelHeight); //std::fill() and std::fill_n()
	}

	memset(m_dRawImageDataFor1DRegrid, 0, sizeof(double) * m_nRawImage_PixelWidth * m_nRawImage_PixelHeight);

}


// EUV PTR 개발코드 부분--------------------------------------------------
int AdamPtr::Command_ScanStart()
{
	int nRet = 0;
	
	AdamDataMemReset();

	Command_EM1024Reset();

	Command_Run();

	ResetEvent(m_hAcquisitionEnd);

	return nRet;
}

int AdamPtr::Command_ScanStop()
{
	int nRet = 0;
	
	int Command_Stop();

	//그냥 넣어 줬음
	m_bIsScaningOn = FALSE;
	m_nImageScanCnt = 0;
	m_nInterpolationCnt = 0;

	m_nFovDataCnt = 0;		//강제 종료후 처음부터 시작하기 위해서 초기화

	WaitSec(1);

	return nRet;
}

unsigned long AdamPtr::CharToUlong(char* buf)
{
	unsigned long out;

	unsigned char* pChar;

	pChar = (unsigned char*)&out;

	for (int i = 0; i < 4; i++)
	{
		pChar[i] = buf[i];
	}
	return out;
}

int AdamPtr::Command_AverageRun()
{
	//이벤트로 값 업데이트 확인하는 부분 추가 필요 ihlee
	int nRet = 0;
	char chBuf[7];

	chBuf[0] = 0x01;

	chBuf[1] = 0x01;
	chBuf[2] = 0x00;
	chBuf[3] = 0x00;
	chBuf[4] = 0x00;

	chBuf[5] = 0x11;
	chBuf[6] = 0x01;

	nRet = SendData(7, chBuf);

	return nRet;
}

int AdamPtr::Command_AverageRunTimeout()
{
	int timeout = 1000;
	//이벤트로 값 업데이트 확인하는 부분 추가 필요 ihlee
	int nRet = 0;
	char chBuf[7];

	chBuf[0] = 0x01;

	chBuf[1] = 0x01;
	chBuf[2] = 0x00;
	chBuf[3] = 0x00;
	chBuf[4] = 0x00;

	chBuf[5] = 0x11;
	chBuf[6] = 0x01;

	nRet = SendData(7, chBuf, timeout, 1);

	return nRet;
}

int AdamPtr::Command_MPacketRun(unsigned int count)
{
	int nRet = 0;

	char chBuf[10];

	chBuf[0] = 0x01;

	chBuf[1] = 0x04;
	chBuf[2] = 0x00;
	chBuf[3] = 0x00;
	chBuf[4] = 0x00;

	chBuf[5] = 0x13;


	unsigned char* p_char = (unsigned char*)&count;

	chBuf[6] = p_char[0];
	chBuf[7] = p_char[1];
	chBuf[8] = p_char[2];
	chBuf[9] = p_char[3];

	nRet = SendData(10, chBuf);

	return nRet;

}

int AdamPtr::Command_MPacketRunTimeout(unsigned int count)
{
	int nRet = 0;
	int timeout = 1000;

	char chBuf[10];

	chBuf[0] = 0x01;

	chBuf[1] = 0x04;
	chBuf[2] = 0x00;
	chBuf[3] = 0x00;
	chBuf[4] = 0x00;

	chBuf[5] = 0x13;

	unsigned char* p_char = (unsigned char*)&count;

	chBuf[6] = p_char[0];
	chBuf[7] = p_char[1];
	chBuf[8] = p_char[2];
	chBuf[9] = p_char[3];

	nRet = SendData(10, chBuf, timeout, 1);

	return nRet;
}


int AdamPtr::Command_SetAverageCount(unsigned int count)
{
	int nRet = 0;

	char chBuf[10];

	chBuf[0] = 0x01;

	chBuf[1] = 0x04;
	chBuf[2] = 0x00;
	chBuf[3] = 0x00;
	chBuf[4] = 0x00;

	chBuf[5] = 0x12;


	unsigned char* p_char = (unsigned char*)&count;

	chBuf[6] = p_char[0];
	chBuf[7] = p_char[1];
	chBuf[8] = p_char[2];
	chBuf[9] = p_char[3];

	nRet = SendData(10, chBuf);

	return nRet;
}


int AdamPtr::Command_Run()
{
	int nRet = 0;
	char chBuf[10];

	chBuf[0] = 0x01;

	chBuf[1] = 0x01;
	chBuf[2] = 0x00;
	chBuf[3] = 0x00;
	chBuf[4] = 0x00;

	chBuf[5] = 0x01;
	chBuf[6] = 0x01;

	nRet = SendData(7, chBuf);

	return nRet;
}

//
//int AdamPtr::Command_ADAMStart()
//{
//	int nRet = 0;
//	char chBuf[10];
//
//	try
//	{
//		if (m_bConnected)
//		{
//
//			chBuf[0] = 0x01;
//			chBuf[1] = 0x00;
//			chBuf[2] = 0x01;
//			chBuf[3] = 0x01;
//			chBuf[4] = 0x01;
//
//			nRet = SendData(5, chBuf);
//		}
//		else
//		{
//			nRet = -1;
//			throw nRet;
//		}
//	}
//	catch (int ex)
//	{
//		// Exception 발생
//		ex;
//	}
//
//	// Exception 발행 해도 이 부분 실행됨(메모리 정리등)
//	return nRet;
//}
int AdamPtr::Command_Stop()
{
	int nRet = 0;
	char chBuf[10];


	chBuf[0] = 0x01;

	chBuf[1] = 0x01;
	chBuf[2] = 0x00;
	chBuf[3] = 0x00;
	chBuf[4] = 0x00;

	chBuf[5] = 0x01;
	chBuf[6] = 0x00;

	nRet = SendData(7, chBuf);

	return nRet;
}

int AdamPtr::ReceiveData(char *lParam, int nLength)
{
	int nRet = 0;
	CString strLog;

#if TRUE
	/// 1. Packet 분할되서 들어올 경우 처리 초기화
	if (m_bReceiveBytesReset == TRUE)
	{
		m_bReceivedBytesSizeTotalUpdated = FALSE;
		m_nReceivedBytesSizeTotal = 0;
		m_nReceivedBytesSizeCurrent = 0;
		m_bReceiveBytesReset = FALSE;
	}

	int m_nReceivedBytesSizePrevious = m_nReceivedBytesSizeCurrent;

	int test = sizeof(int);

	m_nReceivedBytesSizeCurrent = m_nReceivedBytesSizeCurrent + nLength;

	if (m_nReceivedBytesSizeCurrent > ReceiveBufferSize || m_nReceivedBytesSizeCurrent < 0)
	{
		strLog.Format("[Error] m_nReceivedBytesSizeCurrent > ReceiveBufferSize: m_nReceivedBytesSizeCurrent = %d, nLength = %d", m_nReceivedBytesSizeCurrent, nLength);
		SaveLogFile("AdamLog", (LPSTR)(LPCTSTR)strLog);

		//ERROR 처리 2개 PACEKT이 완성되는경우 어떻게 처리?? 일단 리셋함
		m_bReceiveBytesReset = TRUE;

		nRet = -1;
		return nRet;
	}

	//데이터가 이전 데이터가 분할된 경우
	if (m_nReceivedBytesSizePrevious != 0)
	{
		strLog.Format("[Info] Packet Is Devided: m_nReceivedBytesSizePrevious =%d, m_nReceivedBytesSizeCurrent =%d", m_nReceivedBytesSizePrevious, m_nReceivedBytesSizeCurrent);
		SaveLogFile("AdamLog", (LPSTR)(LPCTSTR)strLog);
	}

	memcpy(&m_chReceivedPrebuf[m_nReceivedBytesSizePrevious], lParam, sizeof(byte) * nLength);


	if (m_bReceivedBytesSizeTotalUpdated == FALSE)
	{
		if (m_nReceivedBytesSizeCurrent >= HeadSize)
		{
			// Parsing
			m_nReceivedBytesSizeTotal = (unsigned char)m_chReceivedPrebuf[1] + (unsigned char)m_chReceivedPrebuf[2] * 256;  //E460 = 58464, 1E460 = 124000, 007c 124

			if (m_nReceivedBytesSizeTotal == 58464) 	   //2byte로 data  한계가 있음. 임시 ihlee //packet 1000일경우
			{
				m_nReceivedBytesSizeTotal = NumberOfPacket * PacketSize;
				m_bReceivedBytesSizeTotalUpdated = TRUE;
			}
			else if (m_nReceivedBytesSizeTotal == 124)
			{
				m_bReceivedBytesSizeTotalUpdated = TRUE;
			}
			else if (m_nReceivedBytesSizeTotal == NumberOfPacket * PacketSize) 	   //packet 500 일경우
			{
				m_bReceivedBytesSizeTotalUpdated = TRUE;
			}
			else
			{
				//m_nReceivedBytesSizeTotal = NUMBEROFPACKET * PACKETSIZE;
				//m_bReceivedBytesSizeTotalUpdated = TRUE;

				strLog.Format("[Error] Wrong Packet Size: m_nReceivedBytesSizeTotal= %d", m_nReceivedBytesSizeTotal);
				SaveLogFile("AdamLog", (LPSTR)(LPCTSTR)strLog);

				m_bReceiveBytesReset = TRUE;

				nRet = -1;
				return nRet;
			}
		}
		else
		{
			strLog.Format("[Info] Received size is lower than Header: m_nReceivedBytesSizeCurrent = %d", m_nReceivedBytesSizeCurrent);
			SaveLogFile("AdamLog", (LPSTR)(LPCTSTR)strLog);
		}
	}

	if (m_bReceivedBytesSizeTotalUpdated)
	{
		if (m_nReceivedBytesSizeCurrent == m_nReceivedBytesSizeTotal + HeadSize)
		{
			m_bReceiveBytesReset = TRUE;

			if (m_nReceivedBytesSizeCurrent == PacketSize + HeadSize) //Average Run
			{
				//Average Run Parsing
				memcpy(m_chReceivedbuf[0], m_chReceivedPrebuf + HeadSize, sizeof(byte) * PacketSize);
				ReceivedData_Average(m_chReceivedbuf[0]);
			}
			//else if (m_nReceivedPacketSizeCurrent == ADAM_RECEIVE_BUFFER_SIZE)
			else if (m_nReceivedBytesSizeCurrent == NumberOfPacket * PacketSize + HeadSize)
			{
				int numOfPacket = m_nReceivedBytesSizeTotal / PacketSize;
				if (m_bAverageRunAfter)
				{
					Command_Stop();
					ReceivedData_After(m_chReceivedPrebuf, numOfPacket);
				}
				else
				{
					for (int i = 0; i < numOfPacket; i++)
					{
						memcpy(m_chReceivedbuf[i], m_chReceivedPrebuf + HeadSize + PacketSize * i, sizeof(byte) * PacketSize);

						if (m_nScanType == UniDirection)
						{
							nRet = ReceivedData_UniD(m_chReceivedbuf[i]);
						}
						if (m_nScanType == BiDirection)
						{
							if (m_nEuvImage_Fov == 3000 && m_nEuvImage_ScanGrid == 10 && m_IsUse3umFOV)
							{
								//nRet = ReceivedData_3umOnly(m_chReceivedbuf[i]);
								//nRet = ReceivedData_BiD(m_chReceivedbuf[i]);
							}
							else
							{
								nRet = ReceivedData_BiD(m_chReceivedbuf[i]);
							}
						}
						//if (m_nScanType == ELITHO_DIRECTION)
						//{
						//	//nRet = ReceivedData_Elitho(m_chReceivedbuf[i]);
						//}
					}
				}

			}
		}
		else if (m_nReceivedBytesSizeCurrent > m_nReceivedBytesSizeTotal + HeadSize)
		{
			strLog.Format("[Error] Received size is higher than estimated: m_nReceivedBytesSizeCurrent = %d", m_nReceivedBytesSizeCurrent);
			SaveLogFile("AdamLog", (LPSTR)(LPCTSTR)strLog);
		}
	}


#endif

#if FALSE
	if (nLength == ADAM_RECEIVE_BUFFER_SIZE)
	{
		//버퍼에 복사
		for (int i = 0; i < NUMBEROFPACKET; i++)
		{
			memcpy(m_chReceivedbuf[i], lParam + HEADERSIZE + PACKETSIZE * i, sizeof(byte) * PACKETSIZE);

			if (m_nScanType == UNI_DIRECTION)
			{
				nRet = ReceivedData_UniD(m_chReceivedbuf[i]);
			}
			if (m_nScanType == BI_DIRECTION)
			{
				nRet = ReceivedData_BiD(m_chReceivedbuf[i]);
			}
		}

	}
	else if (nLength == PACKETSIZE + HEADERSIZE) //Average Run
	{
		//Average Run Parsing
		memcpy(m_chReceivedbuf[0], lParam + HEADERSIZE, sizeof(byte) * PACKETSIZE);
		ReceivedData_Average(m_chReceivedbuf[0]);
	}
	else
	{
		int test = 0;
		strLog.Format("[Error] nLength error =%d", nLength);
		SaveLogFile("AdamLog", (LPSTR)(LPCTSTR)strLog);

	}
#endif

	return nRet;
}



int AdamPtr::WaitReceiveEventThread()
{

	int		nRet = 0;
	int		i = 0;
	char*	pInBuf = new char[m_nReceiveMaxBufferSize];
	char*	pOutBuf = new char[m_nReceiveMaxBufferSize];
	//int		total = 0;


	int		nCondition = 0;
	unsigned long	tmp = 0;
	struct timeval timeout;
	fd_set fds;

	SOCKET	tmpSocket = INVALID_SOCKET;
	if (m_bIsServerSocket)
	{
		tmpSocket = m_AcceptSocket;
	}
	else
	{
		tmpSocket = m_Socket;
	}

	//TRACE("TCPIP Receive Thread Start..\n");

	memset(pInBuf, '\0', m_nReceiveMaxBufferSize);
	while (!m_bKillReceiveThread)
	{
		m_bConnected = TRUE;
		timeout.tv_sec = 0;
		timeout.tv_usec = 100;
		FD_ZERO(&fds);
		FD_SET(m_Socket, &fds);
		m_nSocketStatus = select(sizeof(fds) * 8, &fds, NULL, NULL, &timeout);

		if (m_nSocketStatus == SOCKET_ERROR) // m_nSocketStatus=0: Timeout 종종 발생함  ihlee
		{
			CString strLog;
			strLog.Format("[Error]Adam Disconnected: m_nSocketStatus = SOCKET_ERROR");
			SaveLogFile("AdamLog", (LPSTR)(LPCTSTR)strLog);

			m_bKillReceiveThread = TRUE;
			m_bConnected = FALSE;
			nRet = IDC_ERROR_NOT_CONNECTED_TO_MODULE;
			m_nRet = nRet;
			SetEvent(m_hSocketEvent);
		}

		if (tmpSocket != INVALID_SOCKET)
		{
			int bytesReceived = recv(tmpSocket, pInBuf, m_nReceiveMaxBufferSize, NULL);

			if (m_bKillReceiveThread)
			{
				break;
			}

			SetEvent(m_hSocketEvent);
			BOOL isParsing = FALSE;
			if (bytesReceived > 0)
			{
				memcpy(pOutBuf, pInBuf, sizeof(byte) * bytesReceived);
				nRet = ReceiveData(pOutBuf, bytesReceived);
			}
			else
			{
				// 아랫 부분 있으면 통신 THREAD 종료 지연됨, 메모리 릭 발생 ihlee
				//bytesReceived==0 : gracefully closed 접속 해제 상태ihlee 
				CString strLog;
				strLog.Format("[Error]Adam bytesReceived: bytesReceived = %d", bytesReceived);
				SaveLogFile("AdamLog", (LPSTR)(LPCTSTR)strLog);

				m_bKillReceiveThread = TRUE;
				m_bConnected = FALSE;
				nRet = IDC_ERROR_NOT_CONNECTED_TO_MODULE;
				m_nRet = nRet;
				//SetEvent(m_hSocketEvent);			
			}
		}
		else
		{
			if (m_bIsServerSocket)
			{
				m_AcceptSocket = INVALID_SOCKET;
			}
			else
			{
				m_Socket = INVALID_SOCKET;
			}

			m_bConnected = FALSE;
			nRet = IDC_ERROR_NOT_CONNECTED_TO_MODULE;
			m_nRet = nRet;
			SetEvent(m_hSocketEvent);
		}

		//Sleep(1); //김영덕 변경
		//usleep(50); // 이거 왜? ihlee 삭제함
	}

	delete[] pInBuf;
	pInBuf = NULL;
	delete[] pOutBuf;
	pOutBuf = NULL;
	TRACE("TCPIP Receive Thread End..\n");

	return nRet;

}

int AdamPtr::ADAMRunStart()
{
	int nRet = 0;
	CString exceptionMsg;

	// 추가한 초기화 ihlee 2021.03.21
	int TotalScanNum = m_nEuvImage_ScanNumber;//m_MaskMapWnd.m_ProcessData.pMeasureList[i].nRepeatNo;						
	int PointMeasureBufIndex;
	vector <int> vecScanBufIndex;
	//vecScanBufIndex.reserve(TotalScanNum);
	//MemoryAcquireAndInitForPointMeasure(TotalScanNum, PointMeasureBufIndex, vecScanBufIndex);

	PointMeasureBufIndex = 0;
	for (int i = 0; i < m_nEuvImage_ScanNumber; i++)
	{
		vecScanBufIndex.push_back(i);
	}

	//SetPointMeasureBufIndex(PointMeasureBufIndex);
	//SetScanBufIndexVector(vecScanBufIndex); //? 이건 뭐고
	//SetScanBufIndexForReceiveData(vecScanBufIndex); // 이건 뭐임 같은거 같음

	try
	{
		if (!m_bConnected)
		{
			exceptionMsg.Format("Adam is not Connected");
			nRet = -1;
			throw exceptionMsg;
		}

		// UI disable
		//AdamUIControl(FALSE);

		//변수 초기화 
		m_bIsScaningOn = TRUE; //ADAMRunStart
		m_bAbort = FALSE;
		m_bIsFirstScan = FALSE;
		m_nImageScanCnt = 0;
		//CurrentScanNumUiUpdate();

		m_nInterpolationCnt = 0;
		m_nFovDataCnt = 0;

		m_bCanGetData_AtUniDirection = FALSE;									// X Scan
		m_bCanGetData_AtYaxisDirectoin = FALSE;

		// Bi direciton 변수 초기화
		m_bIsScanDirectionForward = FALSE;
		m_nLineNumCnt = 0;
		m_bIsLineScanCompleted = FALSE;
		m_bCanGetData_AtXaxisDirectoin = FALSE;
		m_bIsAcquisitionStart = FALSE;

		m_nReverseDataCnt = 0;
		m_nFovDataCnt_Bi = 0;

		pos_x_current = 0;
		pos_x_old = 0;
		pos_x_oldold = 0;
		pos_y_current = 0;
		pos_y_old = 0;
		pos_y_oldold = 0;

		//Diaplay interpolation초기화
		previousLintCountInterpolation = 0;

		//MutexLock(m_hMutex); //동작 안함  한 함수 내에 있어야 함? 

		if (m_nEuvImage_Fov == 2000)
		{
			//m_dAcelDistance_X = 300.000;			// 숫자 1 이 1 nm 임... ADAM에서 넘어 오는 것도 나노 단위 기준으로 세팅 필요, 정수배로 맞아 떨어져서 pixel 위치가 바뀌는 것을 방지하기위해 넣었음 (m_nEuvImage_ScanGrid/2)
			//m_dAcelDistance_Y = 300.000;			// +(m_nEuvImage_ScanGrid / 2);			// 향후 FOV 크기 맞춰서 조정 필요

			m_dAcelDistance_X = 200.000;			// 숫자 1 이 1 nm 임... ADAM에서 넘어 오는 것도 나노 단위 기준으로 세팅 필요, 정수배로 맞아 떨어져서 pixel 위치가 바뀌는 것을 방지하기위해 넣었음 (m_nEuvImage_ScanGrid/2)
			m_dAcelDistance_Y = 200.000;			// +(m_nEuvImage_ScanGrid / 2);			// 향후 FOV 크기 맞춰서 조정 필요
		}
		else if (m_nEuvImage_Fov == 10000)
		{
			m_dAcelDistance_X = 300.000;			// 
			m_dAcelDistance_Y = 200.000;			// 
		}
		else if (m_nEuvImage_Fov == 3000 && m_nEuvImage_ScanGrid == 10 && m_IsUse3umFOV) // 임시 ihlee 2021.03.17
		{
			m_dAcelDistance_X = 0.0;			// 
			m_dAcelDistance_Y = 0.0;			// 
		}
		else
		{
			m_dAcelDistance_X = 200.000;			// 
			m_dAcelDistance_Y = 200.000;			// 
		}

		// Pixel size를 정의한 부분임
		m_nRawImage_PixelWidth = m_nEuvImage_Fov/* * 1000 */ / m_nEuvImage_ScanGrid;
		m_nRawImage_PixelHeight = m_nEuvImage_Fov/* * 1000 */ / m_nEuvImage_ScanGrid;

		m_nReGridImage_PixelWidth = m_nEuvImage_Fov/* * 1000 */ / m_nEUVImage_InterpolationGrid;
		m_nReGridImage_PixelHeight = m_nEuvImage_Fov/* * 1000 */ / m_nEUVImage_InterpolationGrid;


		if (m_nRawImage_PixelWidth > ADAM_MAX_WIDTH || m_nRawImage_PixelHeight > ADAM_MAX_HEIGHT)
		{
			exceptionMsg.Format("Exceed memory, Check FOV and ScanGrid");
			nRet = -2;
			throw exceptionMsg;
		}

		if (m_nEuvImage_Fov >= m_HighFovCriteria && m_bIsUseHighFovInterpolation == FALSE)
		{
			m_bIsUseInterpolation = FALSE;
		}
		else
		{
			m_bIsUseInterpolation = TRUE;
		}

		if (m_bIsUseInterpolation)
		{
			if (m_nReGridImage_PixelWidth > ADAM_MAX_WIDTH_REGRID || m_nReGridImage_PixelHeight > ADAM_MAX_HEIGHT_REGRID)
			{
				exceptionMsg.Format("Exceed memory, Check FOV and Interpolation Grid");
				nRet = -2;
				throw exceptionMsg;
			}
		}

		//버퍼 초기화 
		m_nOriignaDataI0_Index = 0;
		AdamDataMemReset();



		//InitAdamDlgForScan();

		Command_EM1024Reset();
		Sleep(200);		// Reset 하고 조금 있다가 Stage를 Start 하기 위해서 sleep을 많이 주었음.

		
		//for (int i = 0; i < m_nEuvImage_ScanNumber; i++)
		//{
		//	ResetEvent(m_hAcquisitionEnds[i]);
		//	ResetEvent(m_hRegridEnds[i]);

		//}
		//ResetEvent(m_hAverageEnd);
		//ResetEvent(m_hFilterEnd);

		/* Thread 시작 (거꾸로 실행)*/
		//m_pWaitScanEndThread = AfxBeginThread(WaitScanEndThread, this, THREAD_PRIORITY_NORMAL, 0, 0);//THREAD_PRIORITY_NORMAL		
		//m_pFilterThread = AfxBeginThread(FilterThread, this, THREAD_PRIORITY_NORMAL, 0, 0);//THREAD_PRIORITY_NORMAL, THREAD_PRIORITY_HIGHEST				
		//m_pAverageThread = AfxBeginThread(AverageThread, this, THREAD_PRIORITY_NORMAL, 0, 0);//THREAD_PRIORITY_NORMAL, THREAD_PRIORITY_HIGHEST				

		for (int i = 0; i < m_nEuvImage_ScanNumber; i++)
		{
			//m_pRegridThreads[i] = AfxBeginThread(RegridThread, &m_ThreadParam[i], THREAD_PRIORITY_NORMAL, 0, 0);
		}

		Sleep(200);		//Thread 실행 시간 확보
		//Adam Start
		Command_Run();

	}
	catch (CString exMsg)
	{
		// Exception 발생
		m_bIsScaningOn = FALSE;
		AfxMessageBox("Exception:ADAMRunStart(): " + exMsg);
	}

	return nRet;
}

int AdamPtr::ADAMAbort()
{
	int nRet = 0;

	//Thread 종료 명령
	Command_Stop();
	m_bAbort = TRUE;
	m_IsMeasureComplete = TRUE;

	SaveLogFile("ProcessLogPointMeasure", "Abort");

	//for (int i = 0; i < m_nEuvImage_ScanNumber; i++)
	//{
	//	SetEvent(m_hAcquisitionEnds[i]);
	//}

	m_bIsScaningOn = FALSE;

	return nRet;
}

int AdamPtr::ReceivedData_UniD(char *lParam)		//지금사용
{
	int nRet = 0, i = 0, num = 0, num2 = 0, test = 0;
	double pos_x = 0, pos_y = 0;

	// Data parssing 함수
	Parsing_ADAM_memcpy(lParam);

	//I0 Data Update
	//AdamData.I0_Index = m_nOriignaDataI0_Index;
	//m_dI0_Original[m_nOriignaDataI0_Index++] = m_dParsedData[I0_Detector];

	if (m_nOriignaDataI0_Index >= ORIGINAL_DATA_MAX)
	{
		CString strLog;
		strLog.Format("[Warning] m_dI0_Original Overflow =%u", m_nOriignaDataI0_Index);
		SaveLogFile("AdamLog", (LPSTR)(LPCTSTR)strLog);
		m_nOriignaDataI0_Index = 0;
	}


	if (AdamData.m_nPacketNum == 0)			// ADAM 에서 넘어오는 시작 순서 변경으로 0 확정 _ KYD_20200214
	{
		m_bIsFirstScan = TRUE;
	}
	// 실제 패킷에서 날아온 정보를 메모리에 넣어주는 부분
	if (m_bIsScaningOn == TRUE)	    //Scan 동작 중에만 TRUE
	{
		if (m_bIsFirstScan == TRUE)	//Scan 시작하고 첫 Data를 기억 후 다음 들어오는 값을 Normalization하자
		{
			if (m_IsResetStartPos)
			{
				if (m_IsResetStartPos)
				{
					m_dLaserX_1st = pos_x = AdamData.m_dX_position;
					m_dLaserY_1st = pos_y = AdamData.m_dY_position;
				}

			}
			m_bIsFirstScan = FALSE;
		}
		else
		{
			pos_x = AdamData.m_dX_position - m_dLaserX_1st;	//laser interferometer x axis position data
			pos_y = AdamData.m_dY_position - m_dLaserY_1st;	//laser interferometer y axis position data
		}

		//------------------Y 축 스캐닝 포함하기 위해서 코드 수정함----------------------------------- Start of Section A, 
		// 코드 Hystory에 축만 있는 것도 존재함 필요실 확인 바람_KYD_20200239

		if (pos_x < m_dAcelDistance_X)
		{
			m_bCanGetData_AtUniDirection = TRUE;
		}

		if (pos_y < m_dAcelDistance_Y)
			m_bCanGetData_AtYaxisDirectoin = TRUE;		// Y 축 데이터 획득 구간 정의함2

		if (m_bCanGetData_AtYaxisDirectoin == TRUE)
		{
			if (pos_y >= m_dAcelDistance_Y)
			{
				if (m_bCanGetData_AtUniDirection == TRUE)
				{
					if (pos_x >= m_dAcelDistance_X)
					{
						num = m_nFovDataCnt;

						if (m_nFovDataCnt > (m_nRawImage_PixelWidth - 1))
						{
							num2 = num - m_nRawImage_PixelWidth;

							for (i = 0; i < KindsOfAdamData; i++) // 수정필요
							{
								if (i == Em1024X)
								{
									m_dRawImageForDisplay[i][num2] = pos_x;  // TEST용 삭제해야함, Display? 
									m_dRawImage_Data3D[m_nImageScanCnt % MaxScanMemoryNumber][i][num2] = pos_x;
								}
								else if (i == Em1024Y)
								{
									m_dRawImageForDisplay[i][num2] = pos_y; // TEST용 삭제해야함, Display? 
									m_dRawImage_Data3D[m_nImageScanCnt % MaxScanMemoryNumber][i][num2] = pos_y;
								}
								else
								{
									m_dRawImageForDisplay[i][num2] = m_dParsedData[i]; // TEST용 삭제해야함, Display? 
									m_dRawImage_Data3D[m_nImageScanCnt % MaxScanMemoryNumber][i][num2] = m_dParsedData[i];
								}
							}
							//m_dRawImageForDisplay[I0Index][num2] = AdamData.I0_Index;
							//m_dRawImage_Data3D[m_nImageScanCnt % MaxScanMemoryNumber][I0Index][num2] = AdamData.I0_Index;

						}

						m_nFovDataCnt++;

						m_nRemainPixelCnt = m_nFovDataCnt % m_nRawImage_PixelWidth;

						if (m_nRemainPixelCnt == 0)
						{
							m_bCanGetData_AtUniDirection = FALSE;
							SetEvent(m_hDisplayRawImage);
							//TRACE("Image Line Cnt: %d\n", m_nFovDataCnt / m_nRawImage_PixelWidth);
						}

						if (m_nFovDataCnt == m_nRawImage_PixelWidth * m_nRawImage_PixelHeight + m_nRawImage_PixelHeight)
						{
							m_bCanGetData_AtYaxisDirectoin = FALSE;

							// Image 받아졌으면 이벤트 Signaled로 만들어 Interpolation 수행					
							//SetEvent(m_hAcquisitionEnds[m_nImageScanCnt]);
							m_nImageScanCnt++;

							// Adam 종료
							if (m_nImageScanCnt == m_nEuvImage_ScanNumber)
							{
								//DisplayRawImage();
								Command_Stop();
							}
							else
							{
								//CurrentScanNumUiUpdate();
							}

							m_nFovDataCnt = 0;		// 이게 들어가 줘야 다음번 스캐닝에 다시 0부터 시작하는 것 아닌가?							
						}
					}
				}
			}
		}
	}

	return nRet;
}

int AdamPtr::ReceivedData_BiD(char *lParam)
{
	int nRet = 0, i = 0, num = 0, dataNum = 0, dataNum2 = 0, num2 = 0, num3 = 0, num4 = 0;
	double pos_x = 0, pos_y = 0;
	CString strLog;

	Parsing_ADAM_memcpy(lParam);

	//I0 Data Update
	//AdamData.I0_Index = m_nOriignaDataI0_Index;
	//m_dI0_Original[m_nOriignaDataI0_Index++] = m_dParsedData[I0_Detector];

	if (m_nOriignaDataI0_Index >= ORIGINAL_DATA_MAX)
	{
		CString strLog;
		strLog.Format("[Warning] m_dI0_Original Overflow =%u", m_nOriignaDataI0_Index);
		SaveLogFile("AdamLog", (LPSTR)(LPCTSTR)strLog);
		m_nOriignaDataI0_Index = 0;
	}

	if (AdamData.m_nPacketNum == 0)			// ADAM 에서 넘어오는 시작 순서 변경으로 0 확정 _ KYD_20200214
	{
		m_bIsFirstScan = TRUE;
		m_bIsScanDirectionForward = TRUE;
	}

	if (m_bIsScaningOn == TRUE)	    //Scan 동작 중에만 TRUE
	{
		m_nFovDataCntold = m_nFovDataCnt;

		//처음 스캔 시작시 위치를 정하기 위한 부분 // 일단은 Uni-Direction 과 동일하게
		if (m_bIsFirstScan == TRUE)
		{
			if (m_IsResetStartPos)
			{
				m_dLaserX_1st = pos_x = AdamData.m_dX_position;
				m_dLaserY_1st = pos_y = AdamData.m_dY_position;
			}
			m_bIsFirstScan = FALSE;
		}
		else
		{
			pos_x_current = pos_x = AdamData.m_dX_position - m_dLaserX_1st;	//laser interferometer x axis position data
			pos_y_current = pos_y = AdamData.m_dY_position - m_dLaserY_1st;	//laser interferometer y axis position data
		}

		if ((pos_x_current - pos_x_oldold) > 0)
			m_bIsScanDirectionForward = TRUE;

		if ((pos_x_current - pos_x_oldold) < 0)
			m_bIsScanDirectionForward = FALSE;

		//다시 고민해 볼것....
		if ((m_bIsScanDirectionForward == FALSE) && (pos_x > m_dAcelDistance_X + m_nRawImage_PixelWidth * m_nEuvImage_ScanGrid + 80))
			m_bCanGetData_AtXaxisDirectoin = TRUE;

		if ((m_bIsScanDirectionForward == TRUE) && (pos_x < m_dAcelDistance_X - 80))
			m_bCanGetData_AtXaxisDirectoin = TRUE;

		if (pos_y < m_dAcelDistance_Y)
			m_bCanGetData_AtYaxisDirectoin = TRUE;		// Y 축 데이터 획득 구간 정의함2

		if (m_bCanGetData_AtYaxisDirectoin == TRUE)
		{
			if (m_bCanGetData_AtXaxisDirectoin == TRUE)
			{
				if (pos_y >= m_dAcelDistance_Y)
				{
					if (m_bIsScanDirectionForward == TRUE)
					{
						// Right scan 데이터 획득 코드

						if (pos_x >= m_dAcelDistance_X)
						{
							num = m_nFovDataCnt_Bi;

							if (m_nFovDataCnt_Bi > (m_nRawImage_PixelWidth - 1))
							{
								dataNum = num - m_nRawImage_PixelWidth;

								for (i = 0; i < KindsOfAdamData; i++)
								{
									// LIF NO Reset 전 Bi-ddirection Code
									//m_dRawImage_Data[i][dataNum] = m_dParsedData[i];// TEST용 삭제해야함, Display? 
									//m_dRawImage_Data3D[m_nImageScanCnt % MAX_SCAN_MEMORY_NUMBER][i][dataNum] = m_dParsedData[i];

									if (i == Em1024X)
									{
										m_dRawImageForDisplay[i][dataNum] = pos_x;  // TEST용 삭제해야함, Display? 
										m_dRawImage_Data3D[m_nImageScanCnt % MaxScanMemoryNumber][i][dataNum] = pos_x;
									}
									else if (i == Em1024Y)
									{
										m_dRawImageForDisplay[i][dataNum] = pos_y; // TEST용 삭제해야함, Display? 
										m_dRawImage_Data3D[m_nImageScanCnt % MaxScanMemoryNumber][i][dataNum] = pos_y;
									}
									else
									{
										m_dRawImageForDisplay[i][dataNum] = m_dParsedData[i]; // TEST용 삭제해야함, Display? 
										m_dRawImage_Data3D[m_nImageScanCnt % MaxScanMemoryNumber][i][dataNum] = m_dParsedData[i];
									}

								}

								//m_dRawImageForDisplay[I0Index][dataNum] = AdamData.I0_Index;
								//m_dRawImage_Data3D[m_nImageScanCnt % MaxScanMemoryNumber][I0Index][dataNum] = AdamData.I0_Index;
							}

							m_nFovDataCnt_Bi++;

							m_nRemainPixelCnt = m_nFovDataCnt_Bi % m_nRawImage_PixelWidth;

							if (m_nRemainPixelCnt == 0)
							{
								m_bCanGetData_AtXaxisDirectoin = FALSE;
								SetEvent(m_hDisplayRawImage);
							}
						}
					}

					if (m_bIsScanDirectionForward == FALSE)
					{
						// Left scan 데이터 획득 코드

						if (pos_x <= (m_dAcelDistance_X + (m_nRawImage_PixelWidth * m_nEuvImage_ScanGrid)))		//bi direction 용으로 잠깐 수정 // 1.5에서 잠깐 수정 20은 DDL 적용하고 스테이지 안정화 되면 지워야함 KYD 2021/01/22
						{
							num3 = m_nFovDataCnt_Bi;

							if (m_nFovDataCnt_Bi > (m_nRawImage_PixelWidth - 1))
							{
								num4 = num3 - m_nRawImage_PixelWidth;

								dataNum = num4 + ((m_nRawImage_PixelWidth - 1) - 2 * m_nReverseDataCnt);

								for (i = 0; i < KindsOfAdamData; i++)
								{
									//m_dRawImage_Data[i][dataNum] = m_dParsedData[i];// TEST용 삭제해야함, Display? 
									//m_dRawImage_Data3D[m_nImageScanCnt % MAX_SCAN_MEMORY_NUMBER][i][dataNum] = m_dParsedData[i];

									if (i == Em1024X)
									{
										m_dRawImageForDisplay[i][dataNum] = pos_x;  // TEST용 삭제해야함, Display? 
										m_dRawImage_Data3D[m_nImageScanCnt % MaxScanMemoryNumber][i][dataNum] = pos_x;
									}
									else if (i == Em1024Y)
									{
										m_dRawImageForDisplay[i][dataNum] = pos_y; // TEST용 삭제해야함, Display? 
										m_dRawImage_Data3D[m_nImageScanCnt % MaxScanMemoryNumber][i][dataNum] = pos_y;
									}
									else
									{
										m_dRawImageForDisplay[i][dataNum] = m_dParsedData[i]; // TEST용 삭제해야함, Display? 
										m_dRawImage_Data3D[m_nImageScanCnt % MaxScanMemoryNumber][i][dataNum] = m_dParsedData[i];
									}
								}

								//m_dRawImageForDisplay[I0Index][dataNum] = AdamData.I0_Index;
								//m_dRawImage_Data3D[m_nImageScanCnt % MaxScanMemoryNumber][I0Index][dataNum] = AdamData.I0_Index;

								m_nReverseDataCnt++;

							}

							m_nFovDataCnt_Bi++;


							m_nRemainPixelCnt = m_nFovDataCnt_Bi % m_nRawImage_PixelWidth;

							if (m_nRemainPixelCnt == 0)
							{
								m_bCanGetData_AtXaxisDirectoin = FALSE;
								m_nReverseDataCnt = 0;
								SetEvent(m_hDisplayRawImage);
							}
						}
					}

					if (m_nFovDataCnt_Bi == m_nRawImage_PixelWidth * m_nRawImage_PixelHeight + m_nRawImage_PixelHeight)
					{
						m_bCanGetData_AtYaxisDirectoin = FALSE;

						// Image 받아졌으면 이벤트 Signaled로 만들어 Interpolation 수행										
						//SetEvent(m_hAcquisitionEnds[m_nImageScanCnt]);
						m_nImageScanCnt++;

						// Adam 종료
						if (m_nImageScanCnt == m_nEuvImage_ScanNumber)
						{
							//DisplayRawImage();
							Command_Stop();
						}
						else
						{
							//CurrentScanNumUiUpdate();
						}

						m_nFovDataCnt_Bi = 0;

					}

				}

			}
		}

		pos_x_oldold = pos_x_old;
		pos_y_oldold = pos_y_old;

		pos_x_old = pos_x;
		pos_y_old = pos_y;
	}

	return nRet;
}


int AdamPtr::ReceivedData_Average(char *lParam)   //Bi-direction용 코드 추가 KYD_2020/06/20
{
	int nRet = 0;

	Parsing_ADAM_memcpy(lParam);

	SetEvent(m_hStateOkEvent);

	//CapSensorDataUiUpdate();

	return nRet;
}

int AdamPtr::ReceivedData_After(char *lParam, int num_packet)   //Bi-direction용 코드 추가 KYD_2020/06/20
{
	int nRet = 0;

	double cap1 = 0;
	double cap2 = 0;
	double cap3 = 0;
	double cap4 = 0;


	double LifX = 0;
	double LifY = 0;

	for (int i = 0; i < num_packet; i++)
	{
		memcpy(m_chReceivedbuf[i], lParam + HeadSize + PacketSize * i, sizeof(byte) * PacketSize);
		Parsing_ADAM_memcpy(m_chReceivedbuf[i]);

		//cap1 = cap1 + AdamData.m_dCapsensor1;
		//cap2 = cap2 + AdamData.m_dCapsensor2;
		//cap3 = cap3 + AdamData.m_dCapsensor3;
		//cap4 = cap4 + AdamData.m_dCapsensor4;

		LifX = LifX + AdamData.m_dX_position;
		LifY = LifY + AdamData.m_dY_position;
	}


	//AdamData.m_dCapsensor1 = cap1 / num_packet;
	///AdamData.m_dCapsensor2 = cap2 / num_packet;
	//AdamData.m_dCapsensor3 = cap3 / num_packet;
	//AdamData.m_dCapsensor4 = cap4 / num_packet;
	AdamData.m_dX_position = LifX / num_packet;
	AdamData.m_dY_position = LifY / num_packet;

	m_bAverageRunAfter = FALSE;
	//CapSensorDataUiUpdate();
	//SetEvent(m_hAverageRun);


	return nRet;
}









