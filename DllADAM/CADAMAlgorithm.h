/**
 * ADAM Module Control Dialog Class
 *
 * Copyright 2019 by E-SOL, Inc.,
 *
 */
#pragma once

class CADAMAlgorithm
{
	typedef struct Edge_Point
	{
		int x_pos;		// Edge의 값		
		int y_pos;
	}Edge_Points;

public:
	CADAMAlgorithm();
	~CADAMAlgorithm();

	// Algorithm에 필요한 다양한 함수 및 변수 선언_KYD 20191202~
	double DataRegrid();
	double DataInterpolation();

	int FindEdge_X(double** m_dImagetoEdgeFind, int pixel_x, int pixel_y);
	int FindEdge_Y(double** m_dImagetoEdgeFind, int pixel_x, int pixel_y);
	//int FindEdge(double* m_dImagetoEdgeFind, int pixel_x, int pixel_y);

	void DataLinearRegrid(double** m_dRawImage_Data, double** m_dReconstructedImage_Data, int raw_size_x, int raw_size_y, int reconstructed_size_x, int reconstructed_size_y, double FOV, int num_scans);

	int kkktest(double test);


	// PTR Algorithm
	int Mean(const double data[], int n, double* mean);
	int StandardDeviation(const double data[], int n, double* std, double mean);
	int Statistics(const double data[], int n, double *mean, double* std);
	int test();

	double Min(const double data[], int n);
	double Max(const double data[], int n);

	double mD1 = 0;
	double mD2 = 0;
	double mD3 = 0;

	double mD1Std = 0;
	double mD2Std = 0;
	double mD3Std = 0;

	double mD1BackGround = 1;
	double mD2BackGround = 1;
	double mD3BackGround = 1;
	double mD1BackGraoundStd = 0;
	double mD2BackGraoundStd = 0;
	double mD3BackGraoundStd = 0;

	double mD1RefReflectance = 1;
	double mD2RefReflectance = 1;
	double mD3RefReflectance = 1;
	double mD1RefReflectanceStd = 0;
	double mD2RefReflectanceStd = 0;
	double mD3RefReflectanceStd = 0;

	double mD1WithoutPellicle = 1;
	double mD2WithoutPellicle = 1;
	double mD3WithoutPellicle = 1;
	double mD1WithoutPellicleStd = 0;
	double mD2WithoutPellicleStd = 0;
	double mD3WithoutPellicleStd = 0;

	double mD1WithPellicle = 1;
	double mD2WithPellicle = 1;
	double mD3WithPellicle = 1;
	double mD1WithPellicleStd = 0;
	double mD2WithPellicleStd = 0;
	double mD3WithPellicleStd = 0;


	double mT = 0;
	double mTwithoutNormalization=0;

	double mR = 0;
	double mRbyGainCalculation = 0;
	
	double mRefReflectance = 0.6;
	double mD2ToD1_GainRatioCalculation= 4.7852;
	double mD2ToD1_GainRatioMeasure;

	void CaculatePTR();


};



//-----------------해리스 코너 예제----
	//-------------------------------------------------------------------------


	//vector<vector<double> > gdx2(size_y, vector<double>(size_x, 0.0));
	//vector<vector<double> > gdy2(size_y, vector<double>(size_x, 0.0));
	//vector<vector<double> > gdxy(size_y, vector<double>(size_x, 0.0));
	//
	//float g[5][5] = { { 1, 4, 6, 4, 1 },{ 4, 16, 24, 16, 4 },{ 6, 24, 36, 24, 6 },{ 4, 16, 24, 16, 4 },{ 1, 4, 6, 4, 1 } };
	//
	//for (int y = 0; y < 5; y++)
	//{
	//	for (int x = 0; x < 5; x++)
	//	{
	//		g[y][x] /= 256.f;
	//	}
	//}
	//
	//float tx2, ty2, txy;
	//for (int j = 2; j < size_y - 2; j++)
	//{
	//	for (int i = 2; i < size_x - 2; i++)
	//	{
	//
	//		tx2 = ty2 = txy = 0;
	//		for (int y = 0; y < 5; y++)
	//		{
	//			for (int x = 0; x < 5; x++)
	//			{
	//				tx2 += (dx2[j + y - 2][i + x - 2] * g[y][x]);
	//				ty2 += (dy2[j + y - 2][i + x - 2] * g[y][x]);
	//				txy += (dxy[j + y - 2][i + x - 2] * g[y][x]);
	//			}
	//		}
	//
	//		gdx2[j][i] = tx2;
	//		gdy2[j][i] = ty2;
	//		gdxy[j][i] = txy;
	//		
	//	}
	//
	//}


	//// 미분갑 계산----------------------------------------
	//double tx, ty;
	//vector<vector<double> > dx2(size_y, vector<double>(size_x, 0.0));
	//vector<vector<double> > dy2(size_y, vector<double>(size_x, 0.0));
	//vector<vector<double> > dxy(size_y, vector<double>(size_x, 0.0));

	//for (int j = 1; j < size_y - 1; j++)
	//{
	//	for (int i = 1; i < size_x - 1; i++)
	//	{
	//		tx = (ptr[j - 1][i + 1] + ptr[j][i + 1] + ptr[j + 1][i + 1]
	//			- ptr[j - 1][i - 1] - ptr[j][i - 1] - ptr[j + 1][i - 1]) / 6.f;
	//		ty = (ptr[j + 1][i - 1] + ptr[j + 1][i] + ptr[j + 1][i + 1]
	//			- ptr[j - 1][i - 1] - ptr[j - 1][i] - ptr[j - 1][i + 1]) / 6.f;

	//		dx2[j][i] = tx * tx;
	//		dy2[j][i] = ty * ty;
	//		dxy[j][i] = tx * ty;
	//	}

	//}

	//// 가우시안 필터링----------------------------------------
	//vector<vector<double> > gdx2(size_y, vector<double>(size_x, 0.0));
	//vector<vector<double> > gdy2(size_y, vector<double>(size_x, 0.0));
	//vector<vector<double> > gdxy(size_y, vector<double>(size_x, 0.0));

	//float g[5][5] = { { 1, 4, 6, 4, 1 },{ 4, 16, 24, 16, 4 },{ 6, 24, 36, 24, 6 },{ 4, 16, 24, 16, 4 },{ 1, 4, 6, 4, 1 } };

	//for (int y = 0; y < 5; y++)
	//{
	//	for (int x = 0; x < 5; x++)
	//	{
	//		g[y][x] /= 256.f;
	//	}
	//}

	//float tx2, ty2, txy;
	//for (int j = 2; j < size_y - 2; j++)
	//{
	//	for (int i = 2; i < size_x - 2; i++)
	//	{

	//		tx2 = ty2 = txy = 0;
	//		for (int y = 0; y < 5; y++)
	//		{
	//			for (int x = 0; x < 5; x++)
	//			{
	//				tx2 += (dx2[j + y - 2][i + x - 2] * g[y][x]);
	//				ty2 += (dy2[j + y - 2][i + x - 2] * g[y][x]);
	//				txy += (dxy[j + y - 2][i + x - 2] * g[y][x]);
	//			}
	//		}

	//		gdx2[j][i] = tx2;
	//		gdy2[j][i] = ty2;
	//		gdxy[j][i] = txy;
	//		
	//	}

	//}

	//// 코너 응답 함수 생성

	//vector<vector<double> > crf(size_y, vector<double>(size_x, 0.0));

	//float k = 0.04f;		// 임계점의 값은 알아서 설정함
	//for (int j = 2; j < size_y - 2; j++)
	//{
	//	for (int i = 2; i < size_x - 2; i++)
	//	{
	//		crf[j][i] = (gdx2[j][i] * gdy2[j][i] - gdxy[j][i] * gdxy[j][i])
	//			- k * (gdx2[j][i] + gdy2[j][i])*(gdx2[j][i] + gdy2[j][i]);
	//	}
	//}


	//// 임계점 보다 크면 코너임
	//float cvf_value;
	//vector<int> Corner_X;
	//vector<int> Corner_Y;
	//int xpos = 0;
	//int ypos = 0;
	//double th = 200;

	//for (int j = 2; j < size_y - 2; j++)
	//{
	//	for (int i = 2; i < size_x - 2; i++)
	//	{
	//		cvf_value = crf[j][i];
	//		if (cvf_value > th)
	//		{
	//			if (cvf_value > crf[j - 1][i] && cvf_value > crf[j - 1][i + 1] &&
	//				cvf_value > crf[j][i + 1] && cvf_value > crf[j + 1][i + 1] &&
	//				cvf_value > crf[j + 1][i] && cvf_value > crf[j + 1][i - 1] &&
	//				cvf_value > crf[j][i - 1] && cvf_value > crf[j - 1][i - 1])
	//			{
	//				//Corner_X.push_back(i);
	//				//Corner_Y.push_back(j);
	//				xpos = i;
	//				ypos = j;
	//			}
	//		}
	//	}
	//}