// ESOL Interpolation algorithm made by Roman and Written by YD KIM
// Since 2019/11/01


// 향후 알고리즘 클래스로 모두 이동 예정

#pragma once
#include <vector>

#ifdef _DEBUG
#define _DEBUG_WAS_DEFINED 1
#undef _DEBUG
#endif

#include <Python.h>
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <numpy\arrayobject.h>
#include <numpy\npy_3kcompat.h>

#ifdef _DEBUG_WAS_DEFINED
#define _DEBUG
#endif

using namespace std;
enum source_types {conventional, annular, quadrupole, dipole};


int Griddata(PyObject* pFunc, double** m_dRawImage_Data, double** m_dReconstructedImage_Data, int raw_size_x, int raw_size_y, int reconstructed_size_x, int reconstructed_size_y, int num_scans, double x_start = 0, double y_start = 0, double FOV=2000);
int GriddataNew(PyObject* pFunc, double* Image_raw, double* x_raw, double* y_raw, double* Image_regrid, double* x_regrid, double* y_regrid, int raw_size_x, int raw_size_y, int reconstructed_size_x, int reconstructed_size_y, int num_scans, double x_start, double y_start, double FOV, int tempExtraPixel=0);
int GriddataRemoveNan(PyObject* pFunc, double* Image_raw, double* x_raw, double* y_raw, double* Image_regrid, double* x_regrid, double* y_regrid, int raw_size_x, int raw_size_y, int reconstructed_size_x, int reconstructed_size_y, int num_scans, double x_start, double y_start, double FOV, int tempExtraPixel = 0);
int LowPassFilter(PyObject* pFunc, double** m_dReconstructedImage_Data, int reconstructed_size_x, int reconstructed_size_y, source_types source_type, double f_unit, double sigma_out, double sigma_center, double sigma_radius, double x_step);
int LowPassFilterNew(PyObject* pFunc, double* ImageData, int reconstructed_size_x, int reconstructed_size_y, double x_step, double sigma);


void LinearRegrid(double** m_dRawImage_DataA, double** m_dReconstructedImage_Data, int raw_size_x, int raw_size_y, int reconstructed_size_x, int reconstructed_size_y, double FOV, int num_scans, double x_start = 0, double y_start = 0);
void CubicRegrid(double** m_dRawImage_Data, double** m_dReconstructedImage_Data, int raw_size_x, int raw_size_y, int reconstructed_size_x, int reconstructed_size_y, double FOV, int num_scans, double x_start = 0, double y_start = 0);
void PrepareInterpolationDataFromFile(double** m_dRawImage_Data, int &image_size_x, int &image_size_y, int &num_scans, string input_file_name, HWND m_hWnd, int file_type=0);
double EvaluateCD(vector <double>::iterator aerial_image_begin, vector <double>::iterator aerial_image_end, double threshold, HWND m_hWnd, bool& center_sign);

void EvaluateCD_forLS(vector <vector<double>> input_image, double threshold, double x_step,
double averaging_size, vector<vector<double>>& CD_array, double& average_CD, double & LWR, bool& CD_type, HWND m_hWnd);

void GenerateCDVector(vector <double> cross_section, double threshold, vector<vector<double>>& CD_vector, bool& CD_type, HWND main_window);
void FindEdges(vector <vector<double>> input_image, vector <vector<vector<double>>>& image_edges, double threshold, HWND main_window);
void FindCNTCentersAndLCDU(vector <vector<vector<double>>> image_edges, int cut_size, int image_size_x, int image_size_y, vector<vector<double>>& centers_and_radius, double& CD_X, double& CD_Y, double& LCDU_X, double& LCDU_Y, HWND main_window);
void FilterImage(vector<vector<double>> input_image, vector<vector<double>>& output_image, int cut_size, source_types source_type, double f_unit, double sigma_out, double sigma_center, double sigma_radius, double x_step, HWND main_window);
void FilterImage(double** m_dReconstructedImage_Data, vector<vector<double>>& output_image, int reconstructed_size_x, int reconstructed_size_y, int cut_size, source_types source_type, double f_unit, double sigma_out, double sigma_center, double sigma_radius, double x_step, HWND main_window);


BOOL FindCorner(double** m_dRawImage_DataA, int raw_size_x, int raw_size_y, double th, int* result_x, int* result_y);
BOOL FindCorner_all(double** m_dRawImage_DataA, int raw_size_x, int raw_size_y, double th, int* result_x, int* result_y);

int PythonInitailize();


