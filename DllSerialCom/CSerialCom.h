/**
 * Serial Communication Class
 *
 * Copyright 2019 by E-SOL, Inc.,
 *
 **/
#pragma once

#define	ASCII_LF							0x0a
#define	ASCII_CR							0x0d
#define	ASCII_XON							0x11
#define	ASCII_XOFF							0x13
#define IDS_ERROR_SERIAL_OPENPORT			102
#define IDS_ERROR_SERIAL_TIMER_OUT_EVENT	103
#define IDS_ERROR_SERIAL_SEND				104
#define IDS_ERROR_SERIAL_RECEIVE			105
#define IDC_ERROR_SERIAL_NOT_CONNECTED		106


class AFX_EXT_CLASS CSerialCom : public CECommon
{
public:
	CSerialCom();
	~CSerialCom();

	int						m_nRet;																					//
	CString					m_sPortName;																			// Port Name (COM1 ..)
	WORD					m_wPortID;																				// 
	BOOL					m_bSerialConnected;																		// 
	OVERLAPPED				m_osRead, m_osWrite;																	// Overlapped structure
	HANDLE					m_hComm;																				// Communication Port File Handle
	CString					m_strSendMsg;
	CString					m_strReceivedMsg;
	CString					m_strSendEndOfStreamSymbol;																// 
	CString					m_strReceiveEndOfStreamSymbol;															// 
	BYTE					m_inbuf[512];																			//
	HANDLE					m_hCommThread;																			// Watch Function Thread Handle
	HANDLE					m_hSerialEvent;																			// 

	virtual	int				OpenSerialPort(CString strPortName, DWORD dwBaud, BYTE wByte, BYTE wStop, BYTE wParity);//
	void					ClearSerialPort();																		//
	void					CloseSerialPort();																		//
	int						Send(char *lParam, int nTimeOut = 60000, int nRetryCnt = 3);							//
	int						Send(int nSize, BYTE *lParam, int nTimeOut = 60000, int nRetryCnt = 3);							//
	int						Send(int nSize, char *lParam, int nTimeOut = 60000, int nRetryCnt = 3);
	virtual	int				SendData(char *lParam, int nTimeOut = 60000, int nRetryCnt = 3);						//
	virtual	int				ReceiveData(char *lParam, DWORD dwRead = 0);											// 
	DWORD					WriteComm(BYTE *pBuff, DWORD_PTR nToWrite);													//
	DWORD					ReadComm(BYTE *pBuff, DWORD nToRead);													//
	int						WaitReceiveEventThread();																			//
	static	UINT __cdecl	ReceiveThreadFunc(LPVOID pClass) { ((CSerialCom *)pClass)->WaitReceiveEventThread(); return 0; }	//
};

