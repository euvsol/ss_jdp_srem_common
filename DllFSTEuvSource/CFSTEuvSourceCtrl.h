/**
 * FST EUV Source Control Class
 *
 * Copyright 2019 by E-SOL, Inc.,
 *
 **/

#pragma once
class AFX_EXT_CLASS CFSTEuvSourceCtrl : public CEthernetCom
{
public:
	CFSTEuvSourceCtrl();
	~CFSTEuvSourceCtrl();

	virtual	int	SendData(char *lParam, int nTimeOut = 0, int nRetryCnt = 3);
	virtual	int	ReceiveData(char *lParam); ///< Recive


protected:
	/**
	*@var BOOL m_bEuvOnState
	EUV 발진 확인 변수
	*/
	BOOL m_bEuvOnState;

	BOOL m_bPreviousEuvOnState;

	/**
	*@var BOOL m_bLaserShutterOpenState
	EUV Gate 상태 확인 변수
	*/
	BOOL m_bLaserShutterOpenState;

	BOOL m_bPreviousLaserShutterOpenState;

	/**
	*@var double m_dMainVacuumRate
	Source Main Chamber 진공 값
	*/
	double m_dMainVacuumRate;

	/**
	*@var double m_dSubVacuumRate
	Source Sub Chamber 진공 값
	*/
	double m_dSubVacuumRate;

	/**
	*@var double m_dBufVacuumRate
	Source Buffer Chamber 진공 값
	*/
	double m_dBufVacuumRate;

	/**
	*@var double m_dMFCNeonGauge
	Source MFC Neon 게이지 값
	*/
	double m_dMFCNeonGauge;

	/**
	*@var double m_dNeonGasGauge
	Source Neon Gas 게이지 값
	*/
	double m_dNeonGasGauge;

	/**
	*@var double m_dForelineGauge
	Source Foreline 게이지 값
	*/
	double m_dForelineGauge;

	/**
	*@var double m_dMirrorPosition
	Source 모니터링 CCD를 위한 스테이지 위치
	*/
	double m_dMirrorPosition;

	/**
	*@var BOOL m_bMechShutterOpenState
	Source 메카니컬 셔터 오픈 상태
	*/
	BOOL m_bMechShutterOpenState;

	BOOL m_bPreviousMechShutterOpenState;

	BOOL m_bMonitoringMode;

	/**
	*@fn void ParsingData(CString strRecvMsg)
	*@brief 수신 받은 데이터 파싱
	*@date 2019/07/31
	*@param strRecvMsg 수신받은 메시지 내용
	*/
	void ParsingData(CString strRecvMsg);

	/**
	*@fn int SRC_StartVacuum()
	*@brief Chamber Pumping 시작
	*@date 2019/07/31
	*/
	int	SRC_StartVacuum();

	/**
	*@fn int SRC_StopVacuum()
	*@brief Chamber Venting 시작
	*@date 2019/07/31
	*/
	int	SRC_StopVacuum();

	/**
	*@fn int SRC_StartEuv()
	*@brief EUV On 동작
	*@date 2019/07/31
	*/
	int	SRC_StartEuv();

	/**
	*@fn int SRC_StopEuv()
	*@brief EUV Off 동작
	*@date 2019/07/31
	*/
	int	SRC_StopEuv();

	/**
	*@fn int SRC_OpenMechShutter()
	*@brief 메카니컬 셔터 Open 동작
	*@date 2019/07/31
	*/
	int	SRC_OpenMechShutter();

	/**
	*@fn int SRC_CloseMechShutter()
	*@brief 메카니컬 셔터 Close 동작
	*@date 2019/07/31
	*/
	int	SRC_CloseMechShutter();

	/**
	*@fn int SRC_MoveBeamPassPos()
	*@brief Beam Pass 위치로 이동
	*@date 2019/07/31
	*/
	int	SRC_MoveBeamPassPos();

	/**
	*@fn int SRC_MoveBeamSplitPos()
	*@brief Beam Split 위치로 이동
	*@date 2019/07/31
	*/
	int	SRC_MoveBeamSplitPos();

	/**
	*@fn int SRC_MoveBeamAlignPos()
	*@brief Beam Align 위치로 이동
	*@date 2019/07/31
	*/
	int	SRC_MoveBeamAlignPos();

	/**
	*@fn int SRC_Status()
	*@brief Source 상태 요청
	*@date 2019/07/31
	*/
	int	SRC_Status();

	/**
	*@fn int SRC_SetMirrorPos(double dPos)
	*@brief 미러 스테이지 위치 이동
	*@date 2019/07/31
	*@param 스테이지 이동 위치값 (-13 ~ 13)
	*/
	int	SRC_SetMirrorPos(double dPos);


	enum
	{
		EUV_STATE = 0,
		LASER_SHUTTER_STATE,
		MAIN_VACUUM_DATA,
		SUB_VACUUM_DATA,
		BUFFER_VACUUM_DATA,
		MFC_NEON_DATA,
		NEON_GAS_DATA,
		FORLINE_GAUGE_DATA,
		MIRROR_POS_DATA,
		MECH_SHUTTER_STATE,
		MONITORING_MODE
	};
};

