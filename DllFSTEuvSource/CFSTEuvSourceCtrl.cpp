#include "stdafx.h"
#include "CFSTEuvSourceCtrl.h"

CFSTEuvSourceCtrl::CFSTEuvSourceCtrl()
{
	m_bEuvOnState					 = FALSE;
	m_bPreviousEuvOnState			 = FALSE;
	m_bLaserShutterOpenState	  	 = FALSE;
	m_bPreviousLaserShutterOpenState = FALSE;
	m_bMechShutterOpenState			 = FALSE;
	m_bPreviousMechShutterOpenState  = FALSE;
	m_dMainVacuumRate				 = 0.0;
	m_dSubVacuumRate				 = 0.0;
	m_dBufVacuumRate				 = 0.0;
	m_dMFCNeonGauge					 = 0,0;
	m_dNeonGasGauge					 = 0,0;
	m_dForelineGauge				 = 0,0;
	m_dMirrorPosition				 = 0.0;
	m_bMonitoringMode		= FALSE;

	m_strReceiveEndOfStreamSymbol = _T("\r");
}

CFSTEuvSourceCtrl::~CFSTEuvSourceCtrl()
{
}

int	CFSTEuvSourceCtrl::SendData(char *lParam, int nTimeOut, int nRetryCnt)
{
	int nRet = 0;
	
	m_strSendMsg = lParam;
	SaveLogFile("SrcPc_Com", _T((LPSTR)(LPCTSTR)("PC -> SrcPc : " + m_strSendMsg)));	//통신 상태 기록.
	nRet = Send(lParam, nTimeOut);
	if (nRet != 0)
	{
		CString Temp;
		Temp.Format(_T("PC -> SrcPc : SEND FAIL (%d)"), nRet);
		SaveLogFile("SrcPc_Com", (LPSTR)(LPCTSTR)Temp);	//통신 상태 기록.
	}
	
	return nRet;
}

int CFSTEuvSourceCtrl::ReceiveData(char *lParam)
{
	m_strReceivedMsg = lParam;
	SaveLogFile("SrcPc_Com", _T((LPSTR)(LPCTSTR)("SrcPc -> PC : " + m_strReceivedMsg))); //통신 상태 기록.
	ParsingData(m_strReceivedMsg);

	return 0;
}

void CFSTEuvSourceCtrl::ParsingData(CString strRecvMsg)
{
	if (strRecvMsg == _T("")) return;
	
	CString strTemp;

	AfxExtractSubString(strTemp, strRecvMsg, EUV_STATE, _T(','));
	m_bEuvOnState = strTemp == "EUV On" ? TRUE : FALSE;

	AfxExtractSubString(strTemp, strRecvMsg, LASER_SHUTTER_STATE, _T(','));
	m_bLaserShutterOpenState = strTemp == "EUV Shutter Open" ? TRUE : FALSE;

	AfxExtractSubString(strTemp, strRecvMsg, MAIN_VACUUM_DATA, _T(','));
	m_dMainVacuumRate = atof(strTemp);
	AfxExtractSubString(strTemp, strRecvMsg, SUB_VACUUM_DATA, _T(','));
	m_dSubVacuumRate = atof(strTemp);
	AfxExtractSubString(strTemp, strRecvMsg, BUFFER_VACUUM_DATA, _T(','));
	m_dBufVacuumRate = atof(strTemp);
	AfxExtractSubString(strTemp, strRecvMsg, MFC_NEON_DATA, _T(','));
	m_dMFCNeonGauge = atof(strTemp);
	AfxExtractSubString(strTemp, strRecvMsg, NEON_GAS_DATA, _T(','));
	m_dNeonGasGauge = atof(strTemp);
	AfxExtractSubString(strTemp, strRecvMsg, FORLINE_GAUGE_DATA, _T(','));
	m_dForelineGauge = atof(strTemp);
	AfxExtractSubString(strTemp, strRecvMsg, MIRROR_POS_DATA, _T(','));
	m_dMirrorPosition = atof(strTemp);
	AfxExtractSubString(strTemp, strRecvMsg, MECH_SHUTTER_STATE, _T(','));
	m_bMechShutterOpenState = strTemp == "MECH Shutter Open" ? TRUE : FALSE;

	AfxExtractSubString(strTemp, strRecvMsg, MONITORING_MODE, _T(','));
	m_bMonitoringMode = strTemp == _T("Monitoring On") ? TRUE : FALSE;
}

int CFSTEuvSourceCtrl::SRC_StartVacuum()
{
	int ret = 0;

	CString strCommand = _T("@VACUUM_START") + m_strReceiveEndOfStreamSymbol;
	ret = SendData((LPSTR)(LPCTSTR)strCommand);

	return ret;
}

int CFSTEuvSourceCtrl::SRC_StopVacuum()
{
	int ret = 0;

	CString strCommand = _T("@VACUUM_STOP") + m_strReceiveEndOfStreamSymbol;
	ret = SendData((LPSTR)(LPCTSTR)strCommand);

	return ret;
}

int CFSTEuvSourceCtrl::SRC_StartEuv()
{
	int ret = 0;

	CString strCommand = _T("@EUV_START") + m_strReceiveEndOfStreamSymbol;
	ret = SendData((LPSTR)(LPCTSTR)strCommand);

	return ret;
}

int CFSTEuvSourceCtrl::SRC_StopEuv()
{
	int ret = 0;

	CString strCommand = _T("@EUV_STOP") + m_strReceiveEndOfStreamSymbol;
	ret = SendData((LPSTR)(LPCTSTR)strCommand);

	return ret;
}

int CFSTEuvSourceCtrl::SRC_OpenMechShutter()
{
	int ret = 0;

	CString strCommand = _T("@SHUTTER_OPEN") + m_strReceiveEndOfStreamSymbol;
	ret = SendData((LPSTR)(LPCTSTR)strCommand);

	return ret;
}

int CFSTEuvSourceCtrl::SRC_CloseMechShutter()
{
	int ret = 0;

	CString strCommand = _T("@SHUTTER_CLOSE") + m_strReceiveEndOfStreamSymbol;
	ret = SendData((LPSTR)(LPCTSTR)strCommand);

	return ret;
}

int CFSTEuvSourceCtrl::SRC_MoveBeamPassPos()
{
	int ret = 0;

	CString strCommand = _T("@PI_MOVE_BEAMPASS") + m_strReceiveEndOfStreamSymbol;
	ret = SendData((LPSTR)(LPCTSTR)strCommand);

	return ret;
}

int CFSTEuvSourceCtrl::SRC_MoveBeamSplitPos()
{
	int ret = 0;

	CString strCommand = _T("@PI_MOVE_BEAMSPLIT") + m_strReceiveEndOfStreamSymbol;
	ret = SendData((LPSTR)(LPCTSTR)strCommand);

	return ret;
}

int CFSTEuvSourceCtrl::SRC_MoveBeamAlignPos()
{
	int ret = 0;

	CString strCommand = _T("@PI_MOVE_ALIGN") + m_strReceiveEndOfStreamSymbol;
	ret = SendData((LPSTR)(LPCTSTR)strCommand);

	return ret;
}

int CFSTEuvSourceCtrl::SRC_Status()
{
	int ret = 0;

	CString strCommand = _T("@STATUS") + m_strReceiveEndOfStreamSymbol;
	ret = SendData((LPSTR)(LPCTSTR)strCommand);

	return ret;
}

int CFSTEuvSourceCtrl::SRC_SetMirrorPos(double dPos)
{
	int ret = 0;

	CString sTmp;
	sTmp.Format("%f", dPos);
	CString strCommand = _T("@PI_MOVE_POS,") + sTmp + m_strReceiveEndOfStreamSymbol;
	ret = SendData((LPSTR)(LPCTSTR)strCommand);

	return ret;
}

