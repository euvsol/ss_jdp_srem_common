#include "stdafx.h"
#include "CCrevisIOCtrl.h"


CCrevisIOCtrl::CCrevisIOCtrl()
{
	m_hSystem = NULL;
	m_hDevice = NULL;
}

CCrevisIOCtrl::~CCrevisIOCtrl()
{
}

int CCrevisIOCtrl::OpenDevice(char* pIpAddr)
{
	int ret = 0;

	//
	// Init device
	//
	ret = LibInitSystem(&m_hSystem);

	if (ret != FNIO_ERROR_SUCCESS)
		return ret;

	try
	{
		DEVICEINFOMODBUSTCP2	Devinfo;

		char* pTemp;
		int nIdx = 0;
		pTemp = strtok(pIpAddr, ".");
		Devinfo.IpAddress[nIdx] = atoi(pTemp);
		while (pTemp != NULL)
		{
			nIdx++;
			pTemp = strtok(NULL, ".");
			if (pTemp)
				Devinfo.IpAddress[nIdx] = atoi(pTemp);
		}

		//
		// Open device
		//
		ret = DevOpenDevice(m_hSystem, (FNIO_POINTER)&Devinfo, MODBUS_TCP, &m_hDevice);

		if (ret == FNIO_ERROR_SUCCESS)
		{
			ret = GetDeviceParam();
		}
	}
	catch (...)
	{
		m_hDevice = NULL;

		if (m_hSystem != NULL)
		{
			LibFreeSystem(m_hSystem);
			m_hSystem = NULL;
		}

		return FNIO_ERROR_DEVICE_CONNECT_FAIL;
	}

	return ret;
}

int CCrevisIOCtrl::GetDeviceParam()
{
	if(m_hDevice == NULL)
		return FNIO_ERROR_DEVICE_CONNECT_FAIL;

	int ret = 0;
	FNIO_VALUE	size;

	//
	// Get input image size
	//
	size = sizeof(m_InputSize);
	ret = DevGetParam(m_hDevice, DEV_INPUT_IMAGE_SIZE, DATA_VALUE, (FNIO_POINTER)&m_InputSize, &size);
	if (ret != FNIO_ERROR_SUCCESS)
		return ret;

	//
	// Get output image size
	//
	size = sizeof(m_OutputSize);
	ret = DevGetParam(m_hDevice, DEV_OUTPUT_IMAGE_SIZE, DATA_VALUE, (FNIO_POINTER)&m_OutputSize, &size);
	if (ret != FNIO_ERROR_SUCCESS)
		return ret;

	return ret;
}

int CCrevisIOCtrl::StartIoUpdate()
{
	if (m_hDevice == NULL)
		return FNIO_ERROR_DEVICE_CONNECT_FAIL;

	int ret = 0;

	ret = DevIoUpdateStart(m_hDevice);

	return ret;
}

int CCrevisIOCtrl::StopIoUpdate()
{
	if (m_hDevice == NULL)
		return FNIO_ERROR_DEVICE_CONNECT_FAIL;

	int ret = 0;

	ret = DevIoUpdateStop(m_hDevice);

	return ret;
}

int CCrevisIOCtrl::ReadInputData(BYTE* pInputData)
{
	if (m_hDevice == NULL)
		return FNIO_ERROR_DEVICE_CONNECT_FAIL;

	int ret = 0;

	ret = DevReadInputImage(m_hDevice, 0, pInputData, m_InputSize);

	return ret;
}

int CCrevisIOCtrl::ReadOutputData(BYTE* pOutputData)
{
	if (m_hDevice == NULL)
		return FNIO_ERROR_DEVICE_CONNECT_FAIL;

	int ret = 0;

	ret = DevReadOutputImage(m_hDevice, 0, pOutputData, m_OutputSize);

	return ret;
}

int CCrevisIOCtrl::ReadOutputDataBit(FNIO_VALUE BitIndex, FNIO_VALUE* pBitData)
{
	if (m_hDevice == NULL)
		return FNIO_ERROR_DEVICE_CONNECT_FAIL;

	int ret = 0;

	DevReadOutputImageBit(m_hDevice, 0, BitIndex, pBitData);

	return ret;
}

int CCrevisIOCtrl::ReadInputDataBit(FNIO_VALUE BitIndex, FNIO_VALUE* pBitData)
{
	if (m_hDevice == NULL)
		return FNIO_ERROR_DEVICE_CONNECT_FAIL;

	int ret = 0;

	DevReadInputImageBit(m_hDevice, 0, BitIndex, pBitData);

	return ret;
}

int CCrevisIOCtrl::WriteOutputDataBit(FNIO_VALUE Addr, FNIO_VALUE BitIndex, FNIO_VALUE BitData)
{
	if (m_hDevice == NULL)
		return FNIO_ERROR_DEVICE_CONNECT_FAIL;

	int ret = 0;

	ret = DevWriteOutputImageBit(m_hDevice, Addr, BitIndex, BitData);
		
	return ret;
}


int CCrevisIOCtrl::WriteOutputData(FNIO_VALUE start_Addr, FNIO_POINTER pBuffer, FNIO_VALUE Len)
{
	if (m_hDevice == NULL)
		return FNIO_ERROR_DEVICE_CONNECT_FAIL;

	int ret = 0;

	ret = DevWriteOutputImage(m_hDevice, start_Addr, pBuffer, Len);

	return ret;
}


void CCrevisIOCtrl::CloseDevice()
{
	//
	// Close device
	//
	if(m_hDevice != NULL)
	{
		DevCloseDevice(m_hDevice);
		m_hDevice = NULL;
	}

	//
	// Free system
	//
	if (m_hSystem != NULL)
	{
		LibFreeSystem(m_hSystem);
		m_hSystem = NULL;
	}
}
