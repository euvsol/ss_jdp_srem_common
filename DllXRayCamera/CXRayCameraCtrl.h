/**
 * XRay Camera Control Class
 *
 * Copyright 2019 by E-SOL, Inc.,
 *
 **/
#pragma once
#include "./Includes/picam.h"
#include "./Includes/picam_advanced.h"
#include <vector>
#include <sstream>

typedef struct _XRAY_CAMERA_CONTROL_PARAMETER
{
	//t: type n: int f:  floating point nb: integer �������� boolean

	int tAdcQuaulity;
	double fAdcSpeed;
	int tAdcAnalogGain;
	double fSensorTemperatureSetPoint;
	PicamRoi Roi;
	int tReadoutControlMode;
	int nReadoutPortCount;
	double fExposureTime;
	int tShutterTimingMode;
	double fShutterDelayResolution;
	double fShutterOpeningDelay;
	double fShutterClosingDelay;
	int tTriggerResponse;
	int tTriggerDetermination;
	int tOutputSignal;
	int tOutputSignal2;
	int nbInvertOutputsignal;
	int nbInvertOutputsignal2;
	int nCleanCycleCount;
	int nCleanCycleHeight;
	
	//ROI ImageRoi;
	//double dOperationTemperature;
	//double dNormalTemperature;

} XRAY_CAMERA_CONTROL_PARAMETER;

typedef struct _ROI
{
	piint x;
	piint width;
	piint x_binning;
	piint y;
	piint height;
	piint y_binning;
} ROI;


typedef struct _SUPPLYMENTARY_PARAMETER
{	
	ROI ImageRoi;
	double OperationTemperature;
	double NormalTemperature;

} SUPPLYMENTARY_PARAMETER;

typedef void(*callback)();

class AFX_EXT_CLASS CXRayCameraCtrl : public CECommon
{
public:
	CXRayCameraCtrl();
	~CXRayCameraCtrl();
	

protected:
	std::size_t m_CalculatedBufferSize;  // - calculated buffer size (bytes)
	std::vector<pibyte> m_Buffer;            // - acquisition circular buffer

	piint m_nFramesPerReadout;            // - number of frames in a readout
	piint m_nFrameStride;                 // - stride to next frame (bytes)
	piint m_nFrameSize;                   // - size of frame (bytes)
	piint m_nReadoutStride;               // - stride to next readout (bytes)
	piint m_nImageDataWidth;              // - image data width (pixels)
	piint m_nImageDataHeight;             // - image data height (pixels)
	piint m_nPixelBitDepth;

	pibool m_bHighResolution;   
	HANDLE m_hAcquisitionInactive;            // - event reset during acquisition

	BITMAPINFO* m_pBitmapInfo;	
	PicamCameraID m_IdArray;

	BOOL m_bOnline;

	
	static CXRayCameraCtrl* m_pInstance;
	static BOOL m_bStopped;

	callback CompleteAcquisition;
public:
	BOOL m_bConnected;
	PicamHandle m_hDevice;

	std::vector<pi16u> m_ImageData16;        // - data from last frame (16-bit)
	std::vector<pi32u> m_ImageData32;        // - data from last frame (32-bit)

	
	BOOL OpenXrayCamera(BOOL bOnline);
	BOOL CloseXrayCamera();
	
	void SingleGrab();
	void ContinuousGrab();
	void Start();
	void GrabStop();
	void CommitParam();
	bool SetReadoutCount(bool acquire);

	bool RegisterCameraCallbacks();
	bool InitializeCalculatedBufferSize();
	void CalculateBufferSize(piint readoutStride, piflt onlineReadoutRate);

	int SetTemperatureSetPoint(piflt fValue);
	int SetExposureTime(piflt fValue);
	int GetExposureTime(piflt *fValue);
	int SetAdcQuality(piint nValue);
	int SetAdcSpeed(piflt fValue);
	int SetAdcAnalogGain(piint nValue);
	PicamError SetRoi(PicamRoi roi);
	PicamError GetRoi(PicamRoi *roi);

	PicamError GetXrayCameraControlParameterInDevice();
	PicamError SetXrayCameraControlParameterFromConfig();
	void ReadConfigFile(CString fullfilename);

	float GetTemperature();
	float GetTemperatureSetValue();
	int GetTemperatureStatus();
	int GetCoolingFanStatus();
	int GetAcquisitionRunning();

	void RegisterFunction(callback p_func);

	pibool CacheFrameNavigation();
	pibool InitializeImage();

	std::wstring GetEnumString(PicamEnumeratedType type, piint value);

	void DisplayError(const std::wstring& message, PicamError error = PicamError_None);

	static PicamError PIL_CALL AcquisitionUpdated(PicamHandle device, const PicamAvailableData * available, const PicamAcquisitionStatus * status);

	static CXRayCameraCtrl* GetXrayCameraInstance();

	XRAY_CAMERA_CONTROL_PARAMETER XrayCameraControlParameterInDevice;
	XRAY_CAMERA_CONTROL_PARAMETER XrayCameraControlParameterInConfig;
	SUPPLYMENTARY_PARAMETER SupplymentaryParameter;

	PicamError SetXrayCameraControlParameter(XRAY_CAMERA_CONTROL_PARAMETER SetParameter);

};

